export const indicatorById = {
    "from": {
        "default": [
            "http://data.landportal.info", "http://countries.landportal.info", "http://indicators.landportal.info"
        ],
        "named": []
    },
    "order": [
        {
            "expression": "?time"
        }
    ],
    "prefixes": {
        "cex": "http://purl.org/weso/ontology/computex#",
        "dct": "http://purl.org/dc/terms/",
        "qb": "http://purl.org/linked-data/cube#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "sdmx-attribute": "http://purl.org/linked-data/sdmx/2009/attribute#",
        "time": "http://www.w3.org/2006/time#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "queryType": "SELECT",
    "type": "query",
    "variables": [
        "?indicator",
        "?indicatorName",
        "?country",
        "?countryName",
        "?time",
        "?value",
        "?note"
    ],
    "where": [
        {
            "triples": [
                {
                    "object": "?bindicator",
                    "predicate": "http://purl.org/weso/ontology/computex#ref-indicator",
                    "subject": "?obs"
                }, {
                    "object": "?bcountry",
                    "predicate": "http://purl.org/weso/ontology/computex#ref-area",
                    "subject": "?obs"
                }, {
                    "object": "?btime",
                    "predicate": "http://purl.org/weso/ontology/computex#ref-time",
                    "subject": "?obs"
                }, {
                    "object": "?bvalue",
                    "predicate": "http://purl.org/weso/ontology/computex#value",
                    "subject": "?obs"
                }, {
                    "object": "?revindicatorName",
                    "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                    "subject": "?bindicator"
                }, {
                    "object": "?revcountryName",
                    "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                    "subject": "?bcountry"
                }
            ],
            "type": "bgp"
        }, {
            "patterns": [
                {
                    "triples": [
                        {
                            "object": "?bnote",
                            "predicate": "http://www.w3.org/2000/01/rdf-schema#comment",
                            "subject": "?obs"
                        }
                    ],
                    "type": "bgp"
                }
            ],
            "type": "optional"
        }, {
            "type": "values",
            "values": []
        }, {
            "expression": {
                "args": [
                    {
                        "args": ["?bindicator"],
                        "operator": "str",
                        "type": "operation"
                    },
                    "\"http://data.landportal.info/indicator/\"",
                    "\"\""
                ],
                "operator": "replace",
                "type": "operation"
            },
            "type": "bind",
            "variable": "?indicator"
        }, {
            "expression": {
                "args": ["?revindicatorName"],
                "operator": "str",
                "type": "operation"
            },
            "type": "bind",
            "variable": "?indicatorName"
        }, {
            "expression": {
                "args": [
                    {
                        "args": ["?bcountry"],
                        "operator": "str",
                        "type": "operation"
                    },
                    "\"http://data.landportal.info/geo/\"",
                    "\"\""
                ],
                "operator": "replace",
                "type": "operation"
            },
            "type": "bind",
            "variable": "?country"
        }, {
            "expression": {
                "args": ["?revcountryName"],
                "operator": "str",
                "type": "operation"
            },
            "type": "bind",
            "variable": "?countryName"
        }, {
            "expression": {
                "args": [
                    {
                        "args": ["?btime"],
                        "operator": "str",
                        "type": "operation"
                    },
                    "\"http://data.landportal.info/time/\"",
                    "\"\""
                ],
                "operator": "replace",
                "type": "operation"
            },
            "type": "bind",
            "variable": "?time"
        }, {
            "expression": {
                "args": [
                    {
                        "args": [
                            {
                                "args": [
                                    {
                                        "args": ["?bvalue"],
                                        "operator": "datatype",
                                        "type": "operation"
                                    },
                                    "http://www.w3.org/2001/XMLSchema#string"
                                ],
                                "operator": "=",
                                "type": "operation"
                            }, {
                                "args": ["?bvalue"],
                                "operator": "str",
                                "type": "operation"
                            },
                            "?bvalue"
                        ],
                        "operator": "if",
                        "type": "operation"
                    }
                ],
                "operator": "str",
                "type": "operation"
            },
            "type": "bind",
            "variable": "?value"
        }, {
            "expression": {
                "args": [
                    {
                        "args": [
                            {
                                "args": [
                                    {
                                        "args": ["?bnote"],
                                        "operator": "datatype",
                                        "type": "operation"
                                    },
                                    "http://www.w3.org/2001/XMLSchema#string"
                                ],
                                "operator": "=",
                                "type": "operation"
                            }, {
                                "args": ["?bnote"],
                                "operator": "str",
                                "type": "operation"
                            },
                            "?bnote"
                        ],
                        "operator": "if",
                        "type": "operation"
                    }
                ],
                "operator": "str",
                "type": "operation"
            },
            "type": "bind",
            "variable": "?note"
        }
    ]
}

export const allDatasets = {
    "queryType": "SELECT",
    "variables": [
        "?id", "?label", "?source", "?landvocURI", "?landvoc"
    ],
    "distinct": true,
    "from": {
        "default": [
            "http://datasets.landportal.info", "http://organizations.landportal.info", "http://concepts.landportal.info", "http://themes.landportal.info"
        ],
        "named": []
    },
    "where": [
        {
            "type": "bgp",
            "triples": [
                {
                    "subject": "?dataset",
                    "predicate": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
                    "object": "http://purl.org/linked-data/cube#DataSet"
                }, {
                    "subject": "?dataset",
                    "predicate": "http://www.w3.org/2004/02/skos/core#notation",
                    "object": "?id"
                }, {
                    "subject": "?dataset",
                    "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                    "object": "?label"
                }, {
                    "subject": "?dataset",
                    "predicate": "http://purl.org/dc/terms/publisher",
                    "object": "?pubURI"
                }, {
                    "subject": "?dataset",
                    "predicate": "http://purl.org/dc/terms/subject",
                    "object": "?landvocURI"
                }
            ]
        },
        {
          "type": "optional",
          "patterns": [
            {
              "type": "bgp",
              "triples": [
                {
                  "subject": "?landvocURI",
                  "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                  "object": "?landvoc"
                }
              ]
            }
          ]
        },
        {
          "type": "optional",
          "patterns": [
            {
              "type": "bgp",
              "triples": [
                {
                  "subject": "?pubURI",
                  "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                  "object": "?source"
                }
              ]
            }
          ]
        },
    ],
    "order": [
        {
            "expression": "?label"
        }
    ],
    "type": "query",
    "prefixes": {
        "cex": "http://purl.org/weso/ontology/computex#",
        "time": "http://www.w3.org/2006/time#",
        "qb": "http://purl.org/linked-data/cube#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "dct": "http://purl.org/dc/terms/",
        "sdmx-attribute": "http://purl.org/linked-data/sdmx/2009/attribute#",
        "skos": "http://www.w3.org/2004/02/skos/core#"
    }
}

export const indicatorsByDataset = (dataset) => {

    return {
  "queryType": "SELECT",
  "variables": [
    "?id",
    "?label",
    "?dataset",
    "?datasetLabel",
    "?unit",
    "?description",
    "?landvocURI",
    "?landvoc"
  ],
  "from": {
    "default": [
      "http://indicators.landportal.info",
      "http://datasets.landportal.info",
      "http://themes.landportal.info",
      "http://landvoc.landportal.info"
    ],
    "named": []
  },
  "where": [
    {
      "type": "bgp",
      "triples": [
        {
          "subject": "?uri",
          "predicate": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
          "object": "http://purl.org/weso/ontology/computex#Indicator"
        },
        {
          "subject": "?uri",
          "predicate": "http://www.w3.org/2004/02/skos/core#notation",
          "object": "?id"
        },
        {
          "subject": "?uri",
          "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
          "object": "?label"
        },
        {
          "subject": "?uri",
          "predicate": "http://purl.org/dc/terms/description",
          "object": "?description"
        },
        {
          "subject": "?uri",
          "predicate": "http://purl.org/dc/terms/subject",
          "object": "?landvocURI"
        },
        {
          "subject": "?uri",
          "predicate": "http://purl.org/dc/terms/source",
          "object": "?datasetURL"
        },
        {
          "subject": "?datasetURL",
          "predicate": "http://www.w3.org/2004/02/skos/core#notation",
          "object": "?dataset"
        },
        {
          "subject": "?datasetURL",
          "predicate": "http://www.w3.org/2004/02/skos/core#prefLabel",
          "object": "?datasetLabel"
        }
      ]
    },
    {
      "type": "optional",
      "patterns": [
        {
          "type": "bgp",
          "triples": [
            {
              "subject": "?uri",
              "predicate": "http://purl.org/linked-data/sdmx/2009/attribute#unitMeasure",
              "object": "?unit"
            }
          ]
        }
      ]
    },
    {
      "type": "bgp",
      "triples": [
        {
          "subject": "?landvocURI",
          "predicate": "http://www.w3.org/2004/02/skos/core#prefLabel",
          "object": "?landvoc"
        }
      ]
    },
    {
      "type": "filter",
      "expression": {
        "type": "operation",
        "operator": "=",
        "args": ["?dataset", `\"${dataset}\"`]
      }
  },
  {
 "type": "filter",
 "expression": {
   "type": "operation",
   "operator": "||",
   "args": [
     {
       "type": "operation",
       "operator": "=",
       "args": [
         {
           "type": "operation",
           "operator": "lang",
           "args": [
             "?landvoc"
           ]
         },
         "\"en\""
       ]
     },
     {
       "type": "operation",
       "operator": "=",
       "args": [
         {
           "type": "operation",
           "operator": "lang",
           "args": [
             "?landvoc"
           ]
         },
         "\"\""
       ]
     }
   ]
 }
}
  ],
  "type": "query",
  "prefixes": {
    "cex": "http://purl.org/weso/ontology/computex#",
    "time": "http://www.w3.org/2006/time#",
    "qb": "http://purl.org/linked-data/cube#",
    "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "dct": "http://purl.org/dc/terms/",
    "sdmx-attribute": "http://purl.org/linked-data/sdmx/2009/attribute#",
    "skos": "http://www.w3.org/2004/02/skos/core#"
  }
}

}

export const datasetMetaById = (id) => {

    return {
      "queryType": "SELECT",
      "variables": [
        "?id",
        "?label",
        "?description",
        "?organization",
        "?pubURI",
        "?logo",
        "?license",
        "?landvocURI",
        "?landvoc"
      ],
      "distinct": true,
      "from": {
        "default": [
          "http://datasets.landportal.info",
          "http://organizations.landportal.info",
          "http://themes.landportal.info",
          "http://landvoc.landportal.info"
        ],
        "named": []
      },
      "where": [
        {
          "type": "bgp",
          "triples": [
            {
              "subject": "?dataset",
              "predicate": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
              "object": "http://purl.org/linked-data/cube#DataSet"
            },
            {
              "subject": "?dataset",
              "predicate": "http://www.w3.org/2004/02/skos/core#notation",
              "object": "?id"
            },
            {
              "subject": "?dataset",
              "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
              "object": "?label"
            },
            {
              "subject": "?dataset",
              "predicate": "http://www.w3.org/2000/01/rdf-schema#comment",
              "object": "?description"
            },
            {
              "subject": "?dataset",
              "predicate": "http://purl.org/dc/terms/subject",
              "object": "?landvocURI"
            },
            {
              "subject": "?dataset",
              "predicate": "http://purl.org/dc/terms/publisher",
              "object": "?pubURI"
            }
          ]
        },
        {
          "type": "optional",
          "patterns": [
            {
              "type": "bgp",
              "triples": [
                {
                  "subject": "?pubURI",
                  "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                  "object": "?organization"
                }
              ]
            }
          ]
        },
        {
          "type": "optional",
          "patterns": [
            {
              "type": "bgp",
              "triples": [
                {
                  "subject": "?dataset",
                  "predicate": "http://schema.org/logo",
                  "object": "?logo"
                }
              ]
            }
          ]
        },
        {
          "type": "optional",
          "patterns": [
            {
              "type": "bgp",
              "triples": [
                {
                  "subject": "?dataset",
                  "predicate": "http://purl.org/dc/terms/license",
                  "object": "?license"
                }
              ]
            }
          ]
        },
        {
          "type": "optional",
          "patterns": [
                {
                  "type": "bgp",
                  "triples": [
                    {
                      "subject": "?landvocURI",
                      "predicate": "http://www.w3.org/2004/02/skos/core#prefLabel",
                      "object": "?landvoc"
                    }
                  ]
                },
            ]
        },
        {
          "type": "filter",
          "expression": {
            "type": "operation",
            "operator": "=",
            "args": ["?id", `\"${id}\"`]
          }
      },
      {
     "type": "filter",
     "expression": {
       "type": "operation",
       "operator": "||",
       "args": [
         {
           "type": "operation",
           "operator": "=",
           "args": [
             {
               "type": "operation",
               "operator": "lang",
               "args": [
                 "?landvoc"
               ]
             },
             "\"en\""
           ]
         },
         {
           "type": "operation",
           "operator": "=",
           "args": [
             {
               "type": "operation",
               "operator": "lang",
               "args": [
                 "?landvoc"
               ]
             },
             "\"\""
           ]
         }
       ]
     }
   }
      ],
      "type": "query",
      "prefixes": {
        "cex": "http://purl.org/weso/ontology/computex#",
        "time": "http://www.w3.org/2006/time#",
        "qb": "http://purl.org/linked-data/cube#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "dct": "http://purl.org/dc/terms/",
        "sdmx-attribute": "http://purl.org/linked-data/sdmx/2009/attribute#",
        "skos": "http://www.w3.org/2004/02/skos/core#",
        "schema": "http://schema.org/"
      }
    }

}

export const indicatorMetaById = (id) => {

    return {
        "queryType": "SELECT",
        "variables": [
            "?id", "?label", "?datasetId", "?dataset", "?unit", "?landvoc", "?landvocURI", "?organizationId", "?organization", "?description"
        ],
        "distinct": true,
        "from": {
            "default": [
                "http://indicators.landportal.info", "http://datasets.landportal.info", "http://organizations.landportal.info", "http://oacs.landportal.info", "http://themes.landportal.info"
            ],
            "named": []
        },
        "where": [
            {
                "type": "bgp",
                "triples": [
                    {
                        "subject": "?uri",
                        "predicate": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
                        "object": "http://purl.org/weso/ontology/computex#Indicator"
                    }, {
                        "subject": "?uri",
                        "predicate": "http://www.w3.org/2004/02/skos/core#notation",
                        "object": "?id"
                    }, {
                        "subject": "?uri",
                        "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                        "object": "?label"
                    }, {
                        "subject": "?uri",
                        "predicate": "http://purl.org/dc/terms/description",
                        "object": "?description"
                    }, {
                        "subject": "?uri",
                        "predicate": "http://purl.org/dc/terms/source",
                        "object": "?datasetURL"
                    }, {
                        "subject": "?uri",
                        "predicate": "http://purl.org/dc/terms/subject",
                        "object": "?landvocURI"
                    }, {
                        "subject": "?uri",
                        "predicate": "http://www.w3.org/2000/01/rdf-schema#seeAlso",
                        "object": "?indicatorSeeAlso"
                    }, {
                        "subject": "?datasetURL",
                        "predicate": "http://www.w3.org/2004/02/skos/core#notation",
                        "object": "?datasetId"
                    },{
                        "subject": "?datasetURL",
                        "predicate": "http://www.w3.org/2004/02/skos/core#prefLabel",
                        "object": "?dataset"
                    }, {
                        "subject": "?landvocURI",
                        "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                        "object": "?landvoc"
                    }, {
                        "subject": "?datasetURL",
                        "predicate": "http://purl.org/dc/terms/publisher",
                        "object": "?organizationURL"
                    }, {
                        "subject": "?organizationURL",
                        "predicate": "http://schema.org/url",
                        "object": "?organizationId"
                    },{
                        "subject": "?organizationURL",
                        "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                        "object": "?organization"
                    }
                ]
            },
            {
              "type": "optional",
              "patterns": [
                {
                  "type": "bgp",
                  "triples": [
                    {
                      "subject": "?uri",
                      "predicate": "http://purl.org/linked-data/sdmx/2009/attribute#unitMeasure",
                      "object": "?unit"
                    }
                  ]
                }
              ]
            },
             {
                "type": "filter",
                "expression": {
                    "type": "operation",
                    "operator": "=",
                    "args": ["?id", `\"${id}\"`]
                }
            }
        ],
        "order": [
            {
                "expression": "?label"
            }
        ],
        "type": "query",
        "prefixes": {
            "cex": "http://purl.org/weso/ontology/computex#",
            "time": "http://www.w3.org/2006/time#",
            "qb": "http://purl.org/linked-data/cube#",
            "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
            "dct": "http://purl.org/dc/terms/",
            "sdmx-attribute": "http://purl.org/linked-data/sdmx/2009/attribute#",
            "skos": "http://www.w3.org/2004/02/skos/core#",
            "schema": "http://schema.org/"
        }
    }

}

export const indicatorsAdvanced = () => {

    return {
        "queryType": "SELECT",
        "variables": [
            "?id",
            "?label",
            "?dataset",
            "?unit",
            "?organization",
            "?landvoc",
            "?landvocURI"
        ],
        "from": {
            "default": [
                "http://indicators.landportal.info", "http://datasets.landportal.info", "http://organizations.landportal.info", "http://oacs.landportal.info", "http://themes.landportal.info"
            ],
            "named": []
        },
        "where": [
            {
                "type": "bgp",
                "triples": [
                    {
                        "subject": "?indicatorURI",
                        "predicate": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
                        "object": "http://purl.org/weso/ontology/computex#Indicator"
                    }, {
                        "subject": "?indicatorURI",
                        "predicate": "http://www.w3.org/2004/02/skos/core#notation",
                        "object": "?id"
                    }, {
                        "subject": "?indicatorURI",
                        "predicate": "http://purl.org/dc/terms/subject",
                        "object": "?landvocURI"
                    }, {
                        "subject": "?indicatorURI",
                        "predicate": "http://purl.org/linked-data/sdmx/2009/attribute#unitMeasure",
                        "object": "?unit"
                    }, {
                        "subject": "?indicatorURI",
                        "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                        "object": "?label"
                    }, {
                        "subject": "?indicatorURI",
                        "predicate": "http://purl.org/dc/terms/source",
                        "object": "?datasetURI"
                    }, {
                        "subject": "?datasetURI",
                        "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                        "object": "?dataset"
                    }, {
                        "subject": "?datasetURI",
                        "predicate": "http://purl.org/dc/terms/publisher",
                        "object": "?sourceOrgURI"
                    }, {
                        "subject": "?sourceOrgURI",
                        "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                        "object": "?organization"
                    }, {
                        "subject": "?landvocURI",
                        "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
                        "object": "?landvoc"
                    }
                ]
            }
        ],
        "order": [
            {
                "expression": "?label"
            }
        ],
        "type": "query",
        "prefixes": {
            "cex": "http://purl.org/weso/ontology/computex#",
            "skos": "http://www.w3.org/2004/02/skos/core#",
            "dct": "http://purl.org/dc/terms/",
            "sdmx-attribute": "http://purl.org/linked-data/sdmx/2009/attribute#",
            "rdfs": "http://www.w3.org/2000/01/rdf-schema#"
        }
    }

}

export const countriesByIndicator = (id) => {

    return {
      "queryType": "SELECT",
      "variables": [
        "?country"
      ],
      "distinct": true,
      "from": {
        "default": [
          "http://data.landportal.info",
          "http://countries.landportal.info",
          "http://indicators.landportal.info"
        ],
        "named": []
      },
      "where": [
        {
          "type": "bgp",
          "triples": [
            {
              "subject": "?obs",
              "predicate": "http://purl.org/weso/ontology/computex#ref-indicator",
              "object": "?bindicator"
            },
            {
              "subject": "?obs",
              "predicate": "http://purl.org/weso/ontology/computex#ref-area",
              "object": "?bcountry"
            }
          ]
        },
        {
          "type": "values",
          "values": [
            {
              "?bindicator": `http://data.landportal.info/indicator/${id}`
            }
          ]
        },
        {
          "type": "bind",
          "variable": "?country",
          "expression": {
            "type": "operation",
            "operator": "replace",
            "args": [
              {
                "type": "operation",
                "operator": "str",
                "args": [
                  "?bcountry"
                ]
              },
              "\"http://data.landportal.info/geo/\"",
              "\"\""
            ]
          }
        }
      ],
      "type": "query",
      "prefixes": {
        "cex": "http://purl.org/weso/ontology/computex#",
        "time": "http://www.w3.org/2006/time#",
        "qb": "http://purl.org/linked-data/cube#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "dct": "http://purl.org/dc/terms/",
        "sdmx-attribute": "http://purl.org/linked-data/sdmx/2009/attribute#"
      }
    }
}

export const currentThemes = () => {
     return {
    	"queryType": "SELECT",
    	"variables": [
    		"*"
    	],
    	"from": {
    		"default": [
    			"http://themes.landportal.info"
    		],
    		"named": []
    	},
    	"where": [
    		{
    			"type": "bgp",
    			"triples": [
    				{
    					"subject": "?theme",
    					"predicate": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
    					"object": "http://www.w3.org/2004/02/skos/core#Concept"
    				},
    				{
    					"subject": "?theme",
    					"predicate": "http://www.w3.org/2004/02/skos/core#notation",
    					"object": "?id"
    				},
    				{
    					"subject": "?theme",
    					"predicate": "http://www.w3.org/2004/02/skos/core#prefLabel",
    					"object": "?label"
    				}
    			]
    		}
    	],
    	"type": "query",
    	"prefixes": {
    		"skos": "http://www.w3.org/2004/02/skos/core#"
    	}
    }
}

export const currentConcepts = () => {

    return {
    	"queryType": "SELECT",
    	"variables": [
    		"*"
    	],
    	"from": {
    		"default": [
    			"http://landvoc.landportal.info"
    		],
    		"named": []
    	},
    	"where": [
    		{
    			"type": "bgp",
    			"triples": [
    				{
    					"subject": "?concept",
    					"predicate": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
    					"object": "http://www.w3.org/2004/02/skos/core#Concept"
    				},
    				{
    					"subject": "?concept",
    					"predicate": "http://www.w3.org/2004/02/skos/core#prefLabel",
    					"object": "?label"
    				}
    			]
    		},
    		{
    			"type": "filter",
    			"expression": {
    				"type": "operation",
    				"operator": "=",
    				"args": [
    					{
    						"type": "operation",
    						"operator": "lang",
    						"args": [
    							"?label"
    						]
    					},
    					"\"en\""
    				]
    			}
    		}
    	],
    	"type": "query",
    	"prefixes": {
    		"skos": "http://www.w3.org/2004/02/skos/core#"
    	}
    }
}
