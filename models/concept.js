import mongoose, { Schema } from 'mongoose';

const ConceptSchema = new Schema({

    label: {
        type: String,
        required: true,
    },
    url: String,


});

export default mongoose.model('Concept', ConceptSchema);
