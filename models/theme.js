import mongoose, { Schema } from 'mongoose';

const ThemeSchema = new Schema({

    id: {
        type: String,
        required: true,
        unique: true,
    },
    label: {
        type: String,
        required: true,
    },
    url: String,
    image: {
        data: Buffer,
        contentType: String,
    }

});

export default mongoose.model('Theme', ThemeSchema);
