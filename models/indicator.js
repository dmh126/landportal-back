import mongoose, { Schema } from 'mongoose';

const IndicatorSchema = new Schema({

    name: {
      type: String,
      required: true,
    },
    about: {
      type: String,
    },
    dataset: {
        type: Schema.Types.ObjectId,
        ref: 'Dataset'
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    updated_at: {
      type: Date,
      default: Date.now,
    },
    type: {
      type: String,
      required: true,
      enum: ['Vector', 'Raster', 'WMS', 'SPARQL'],
    },
    bbox: {
      type: [String],
    },

    region: {
      type: String,
      default: ''
    },
    country: {
      type: String,
      default: ''
    },
    /* Vector properties */
    vector: {
      source_url: {
        type: String,
      },
      format: {
        type: String,
      },
      geometry: {
        type: String,
        enum: ['Point', 'Line', 'Polygon', 'MultiPoint', 'MultiLine', 'MultiPolygon'],
      },
      encoding: {
        type: String,
      },
    },
    /* Raster properties */
    raster: {
      source_url: {
        type: String,
      },
      format: {
        type: String,
      },
    },
    /* WMS properties */
    wms: {
      source_url: {
        type: String,
      },
      layer: {
        type: String,
      },
    },
    sparql: {
        source_url: {
            type: String,
        },
    },

    /* Metadata */
    meta: {
      // https://landportal.org/book/reuse
      indicator: {
          label: { type: String },
          description: { type: String },
          picture: { type: String },
          dataset: { type: String },
          id: { type: String },
          min: { type: Number },
          max: { type: Number },
          measurement_unit: { type: String },
          has_coded_value: { type: String },
          high_low: { type: String },
          related_overarching_categories: { type: String, default: '' },
          related_themes: { type: String, default: '' },
          related_landvoc_concepts: { type: String, default: '' },
      }
      // additional
  },

  // hide
  hidden: {
      type: Boolean
  }

});

export default mongoose.model('Indicator', IndicatorSchema);
