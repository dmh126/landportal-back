import mongoose, { Schema } from 'mongoose';

const StyleSchema = new Schema({

    indicator: {
      type: String,
      required: true
    },

    type: { // visualization type
      type: String,
      required: true,
      enum: ['Graduated Colors', 'Heatmap', 'Proportional'],
    },

    properties: {
        opacity: Number, // all
        palette: String, // graduated
        color: String, // proportional
        shape: String, // proportional
        radius: Number, // heatmap
        blur: Number // heatmap
    },

    hide: {
        type: Boolean,
        default: false,
    },

    categorical: {
        type: Schema.Types.Mixed
    },
    allowed: {
        type: [String],
        default: ['Graduated Colors', 'Heatmap', 'Proportional'],
    }

});

export default mongoose.model('Style', StyleSchema);
