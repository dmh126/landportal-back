import mongoose, { Schema } from 'mongoose';

const MapCompositionSchema = new Schema({

    basemap: String,
    extent: [Number],
    indicators: [
        {
            id: String,
            type: {type: String},
            style: Object,
            visible: Boolean,
        }
    ]

});

export default mongoose.model('MapComposition', MapCompositionSchema);
