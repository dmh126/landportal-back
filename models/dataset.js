import mongoose, { Schema } from 'mongoose';

const DatasetSchema = new Schema({

    name: {
      type: String,
      required: true,
    },
    about: {
      type: String,
      required: true,
    },
    created_at: {
      type: Date,
      default: Date.now,
    },
    updated_at: {
      type: Date,
      default: Date.now,
    },
    bbox: {
      type: [String],
    },
    reference: {
        type: String,
    },

    /* Metadata */
    meta: {
      // https://landportal.org/book/reuse
      dataset: {
          label: { type: String },
          description: { type: String },
          id: { type: String },
          logo: {
              data: Buffer,
              contentType: String,
              url: String,
          },
          license: { type: String },
          copyright_details: { type: String },
          organization: { type: String },
          organizationUri: {type: String},
          related_overarching_categories: { type: String },
          related_themes: { type: String },
          related_landvoc_concepts: { type: String, default: '' },
      }
    }

});

export default mongoose.model('Dataset', DatasetSchema);
