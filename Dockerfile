FROM node:8
RUN apt-get update
RUN apt-get install -y binutils libproj-dev gdal-bin
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]
