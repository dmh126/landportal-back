# Landportal Back-End app

Requirements:
```
.env file must be placed inside the root dir
```

Development how To:

```
npm install

npm run development
```

Deployment how to:
```
docker build --tag=<app_name> .
```

Contact:

```
harasymczuk@contecht.eu
```
