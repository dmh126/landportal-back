const config = {};
config.hostname = process.env.SERVER_URL;
config.front = process.env.CLIENT_URL;
config.database = {
  url: process.env.DB_HOST,
  secret: process.env.DB_SECRET,
};
/*
config.aws = {

};
config.jwtSecret = '';
*/

config.postgis = {
    host: process.env.PG_HOST,
    port: process.env.PG_PORT,
    dbname: process.env.PG_DBNAME,
    user: process.env.PG_USER,
    password: process.env.PG_PASSWORD,
};

config.geoserver = {
    url: process.env.GEO_URL,
    user: process.env.GEO_USER,
    password: process.env.GEO_PASSWORD,
    namespace: process.env.GEO_NAMESPACE,
    store: process.env.GEO_STORE,
}

export default config;
