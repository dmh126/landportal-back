const config = {};
config.hostname = process.env.SERVER_URL;
config.front = process.env.CLIENT_URL;
config.database = {
  url: process.env.DB_HOST,
  secret: process.env.DB_SECRET,
};
/*
config.aws = {

};
config.jwtSecret = '';
*/

export default config;
