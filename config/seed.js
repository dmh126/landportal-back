/*


  A SEED FILE, IT GENERATES A DEFAULT ADMIN IN THE DATABASE ID DOESN'T EXIST.
  SHOULD BE USED ONLY IN DEVELOPMENT AND TESTING ENVIRONMENTS, DO NOT CALL IT IN PRODUCTION.

  Credentials

    admin@contecht.eu:admin

*/

import User from '../models/user';

export default function generateSeed () {

  User.findOne({ email: 'admin@contecht.eu' })
    .then( res => {

      if( res ) {

        console.log('Admin already exists!');

      } else {

        let user = new User();
        user.name = 'AutoAdmin';
        user.email = 'admin@contecht.eu';
        user.password = user.generateHash('admin');
        user.isAdmin = true;

        user.save( (err, a) => {

          if (err) {

            console.log('Admin cant be created!')

          } else {

            console.log('Admin created!')

          }

        })
      }

    })
    .catch( error => {

      console.log('Something went wrong!')

    })

}
