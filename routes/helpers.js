import axios from 'axios';
import _ from 'lodash';
import config from '../config/config';
import {BBOXES} from '../constants/bboxes.js';
//import {THEMES, CONCEPTS} from '../constants/LandVoc.js';
import Indicator from '../models/indicator';
import Theme from '../models/theme';
import Concept from '../models/concept';
import {getThemes, getConcepts} from './sparql.js';


export const getLayers = async(req, res) => {

    try {
        const result = await grabLayers();
        res.json(result);
    } catch (e) {
        res.json({ok: 0})
    }

}

export const getLayerInfo = (req, res) => {

    const geoserver = config.geoserver;
    const layer_name = req.params.layer;

    axios({
        method: 'GET',
        url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/layers/${layer_name}.json`,
        auth: {
            username: geoserver.user,
            password: geoserver.password
        }
    })
    .then( result => {

        const layer = result.data.layer;

        let url;
        let type = layer.type;
        let name = layer.name;

        switch (layer.type) {

            case "VECTOR":
                url = `${geoserver.url}/geoserver/${geoserver.namespace}/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=landportal%3A${layer.name}&outputFormat=application%2Fjson`;
                break;

            case "WMS":
                url = `${geoserver.url}/geoserver/${geoserver.namespace}/wms?service=WMS&version=1.1.0&request=GetMap&layers=landportal%3A${layer.name}&srs=EPSG%3A4326`;
                break;

            default:
                url = "error";
                break

        };


        res.json({ok: 1, url, type, name});

    })
    .catch( e => {
        console.log(e)

        res.json({ok: 0})

    })

}

export const checkCountries = (bbox) => {

    let countries = [];

    const keys = Object.keys(BBOXES);

    keys.forEach( key => {

        const item = BBOXES[key];

        const intersection = intersect(item, bbox);
        if (intersection) {
            countries.push(key)
        }

    })

    return countries;

}

const intersect = (a, b) => {

  return (b.min.lon < a.max.lon &&
           b.max.lon > a.min.lon &&
           b.max.lat > a.min.lat &&
           b.min.lat < a.max.lat);

}

export const fixTagsSemicolons = async(tags, type) => {

    let new_tags = [];

    if (tags) {
        if (typeof tags === 'string')
            tags = [tags];

        if (type === 'themes') {

            let themes = await Theme.find({});
            themes = themes.map(t => t.label);
            themes.forEach( i => {
                if (tags.includes(i)) {
                    new_tags.push(i);
                }
            })

        } else if (type === 'concepts') {

            let concepts = await Concept.find({});
            concepts = concepts.map(t => t.label);
            concepts.forEach( i => {
                if (tags.includes(i)) {
                    console.log(i)
                    new_tags.push(i);
                }
            })

        }
    }

    return new_tags.join(';')
}

// THEMES AND CONCEPTS

export const updateThemes = async (req, res) => {

    let newThemes = await getThemes();
    newThemes = newThemes.map( t => {
        return {
            id: t.id.value,
            label: t.label.value,
            url: t.theme.value
        }
    });

    let oldThemes = await Theme.find();

    let updatedThemes = _.unionWith(newThemes, oldThemes, (x,y) => x.id === y.id )

    await Theme.remove();

    Theme.insertMany(newThemes)
    .then( r => {
        res.json({ok: 1})
    })
    .catch( e => {
        res.json({ok: 0})
    })

}

export const addThemePicture = (req, res) => {

    const file = req.files.image;
    const id = req.body.id;
    if (!file) {
        return res.json({ok: 0});
    }
    const image = {
        data: file.data,
        contentType: file.mimetype
    };

    Theme.findOneAndUpdate({id}, { $set: {image}})
    .then(result => {
        res.json({ok: 1});
    })
    .catch(e => {
        res.json({ok: 0})
    });

}

export const updateConcepts = async(req, res) => {

    let newConcepts = await getConcepts();
    newConcepts = newConcepts.map( t => {
        return {
            label: t.label.value,
            url: t.concept.value
        }
    })

    if (newConcepts.length) {
        await Concept.remove();
    }

    Concept.insertMany(newConcepts)
    .then( r => {
        res.json({ok: 1})
    })
    .catch( e => {
        res.json({ok: 0})
    })

}

export const grabThemes = async(req, res) => {

    let t = await Theme.find({});
    t = t.map(i => i.label).sort();

    res.json({themes: t});
}

export const grabConcepts = async(req, res) => {

    let c = await Concept.find({});
    c = c.map(i => i.label).sort();

    res.json({concepts: c});
}

export const grabLayers = () => {

    const geoserver = config.geoserver;

    return new Promise( (resolve, reject) => {
        axios({
            method: 'GET',
            url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/layers`,
            auth: {
                username: geoserver.user,
                password: geoserver.password
            }
        })
        .then( layers => {

            let result = [];

            layers.data.layers.layer.map( item => {

                let name = String(item.name);

                if (!name.includes('country_')) {
                    result.push(name)
                }

            })

            resolve(result);

        })
        .catch( e => {
            reject(null)
        })

    });

}
