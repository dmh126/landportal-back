import Dataset from '../models/dataset';
import Indicator from '../models/indicator';
import Style from '../models/style';
import Concept from '../models/concept';
import Theme from '../models/theme';
import config from '../config/config';
const fs = require('fs');
const flat = require('flat');

import * as helpers from './files';
import * as wms from './wms';
import {fixTagsSemicolons, grabLayers} from './helpers'

import {getAllDatasetsBySparql, getIndicatorsBySparql, getIndicatorCountries} from './sparql'
import {buildDatasetsList, buildIndicatorsList} from './datasets';

import {CONCEPTS} from '../constants/LandVoc';
import {COUNTRIES_ISO, REGIONS, COUNTRIES} from '../constants/countries';

export const login = (req, res) => {

    if (req.user) {
        res.redirect('/admin/datasets')
    } else {
        res.render('../views/pages/index');
    }

}

export const authorization = (req, res) => {

    res.redirect('/admin/datasets');

}

export const settings = (req, res) => {

    res.render('../views/pages/settings', {
        error: false,
        message: ""
    });

}

export const isLoggedIn = (req, res, next) => {

    if (req.user) {
        next();
    } else {
        res.redirect('/admin')
    }

}

export const logOut = (req, res) => {

    req.logOut();
    res.redirect('/admin');

}

export const datasets = async(req, res) => {

    let result = await Dataset.find({});
    //let drupal = await getAllDatasetsBySparql();
    //drupal = buildDatasetsList(drupal);
    /*drupal = drupal.map( item => {
        return {
            name: item.label.value,
            id: item.id.value,
        }
    });*/
    //const result = mongo.concat(drupal);
    res.render('../views/pages/datasets', {
        datasets: result,
        client_url: config.front
    });



}

export const indicators = async(req, res) => {

    const id = req.params.dataset_id;

    if (id.match(/^[0-9a-fA-F]{24}$/)) { // id is a mongo _id

        let indicators = await Indicator.find({dataset: id});
        let dataset = await Dataset.findOne({_id: id});

        res.render('../views/pages/indicators-mongo', {
            indicators: indicators,
            client_url: config.front,
            dataset: (dataset) ? dataset.name : 'unknown',
            dataset_id: req.params.dataset_id
        });


    } else { // else it's from sparql

        let indicators = await getIndicatorsBySparql(id);
        let dataset_name;
        if (indicators && indicators.length) {
            dataset_name = indicators[0].datasetName.value;
        }
        indicators = indicators.map(item => {

            return {
                name: item.label.value,
                type: "SPARQL",
                sparql: {
                    source_url: item.id.value,
                },
                id: item.id.value,
                hide: "no"
            }

        });

        let ids = indicators.map( item => item.id);
        let style = await Style.find({indicator: {"$in": ids}});

        style.forEach( s => {
            let indicator = indicators.find( i => s.indicator === i.id);
            if (indicator) {
                indicator.hide = (s.hide) ? "yes" : "no"
            }
        })

        res.render('../views/pages/indicators-drupal', {
            indicators: indicators,
            client_url: config.front,
            dataset: dataset_name,
            dataset_id: req.params.dataset_id
        });

    }

}

export const indicatorsOverview = async(req, res) => {

        //let indicators = await Indicator.find().populate('dataset', 'name');
        let indicators = await Indicator.aggregate([

                {'$lookup': {
                    'from': 'datasets',
                    'localField': 'dataset',
                    'foreignField': '_id',
                    'as': 'dataset'
                }},
                {'$unwind': '$dataset'}

        ])

        res.render('../views/pages/indicators-overview', {
            indicators: indicators,
            client_url: config.front,
        });

}

export const hideIndicator = (req, res) => {
    const id = req.params.id;
    const hide = (req.body.hide === "yes") ? true : false;

    Style.update({indicator: id}, {hide: hide}, r => {
        res.json(hide)
    })
    Indicator.update({_id: id}, {hidden: hide}, r => {
        console.log(r)
    })
}

export const editDataset = async(req, res) => {

    let datasets = await getAllDatasetsBySparql();
    datasets = buildDatasetsList(datasets);
    let concepts = await Concept.find({});
    concepts = concepts.map(i => i.label).sort();
    let themes = await Theme.find({});
    themes = themes.map(i => i.label).sort();

    Dataset.findOne({_id: req.params.id}).then(dataset => {

        if (dataset) {
            let logo = dataset.meta.dataset.logo;
            if (logo && logo.data) {
                dataset.meta.dataset.dataset_logo = `data:${logo.contentType};base64, ${logo.data.toString('base64')}`;
            }
            res.render('../views/pages/edit-dataset', {
                dataset: dataset,
                concepts,
                themes,
                datasets: datasets,
            });
        } else {
            res.render(404);
        }
    })

}

export const editIndicator = async (req, res) => {

    let concepts = await Concept.find({});
    concepts = concepts.map(i => i.label).sort();
    let themes = await Theme.find({});
    themes = themes.map(i => i.label).sort();

    Indicator.findOne({_id: req.params.indicator_id}).then(indicator => {

        if (indicator) {
            res.render('../views/pages/edit-indicator', {
                indicator: indicator,
                dataset_id: req.params.dataset_id,
                concepts,
                themes,
                regions: REGIONS,
                countries: COUNTRIES,
                geo_url: config.geoserver.url,
                geo_namespace: config.geoserver.namespace
            });
        } else {
            res.render(404);
        }
    })

}

export const newDataset = async(req, res) => {

    let datasets = await getAllDatasetsBySparql();
    datasets = buildDatasetsList(datasets);
    let concepts = await Concept.find({});
    concepts = concepts.map(i => i.label).sort();
    let themes = await Theme.find({});
    themes = themes.map(i => i.label).sort();

    res.render('../views/pages/new-dataset', {concepts, themes, datasets});

}

export const newIndicator = async(req, res) => {

    const id = req.params.dataset_id;

    const ds = await Dataset.findOne({_id: id}, {name: 1});

    let concepts = await Concept.find({});
    concepts = concepts.map(i => i.label).sort();
    let themes = await Theme.find({});
    themes = themes.map(i => i.label).sort();

    res.render('../views/pages/new-indicator', {
        dataset_id: id,
        dataset_name: ds.name,
        concepts,
        themes,
        regions: REGIONS,
        countries: COUNTRIES,
        geo_url: config.geoserver.url,
        geo_namespace: config.geoserver.namespace
    });

}

export const submitDataset = async(req, res) => {

    let file = req.files.dataset_logo_file;
    let url = req.body.dataset_logo_url;
    let image = {};
    if (file) {
        image.data = file.data;
        image.contentType = file.mimetype;
    }
    if (url) {
        image.url = url;
    }

    const form = req.body;
    form.dataset_logo = image;
    // fix tags
    form.dataset_related_themes = await fixTagsSemicolons(form.dataset_related_themes, 'themes');
    form.dataset_related_landvoc_concepts = await fixTagsSemicolons(form.dataset_related_landvoc_concepts, 'concepts');
    const ds = buildDataset(form);

    const dataset = new Dataset(ds);

    const reference = dataset.reference;
    let indicators = await getIndicatorsBySparql(reference);
    indicators = buildIndicatorsList(indicators);
    /*
    dataset.save().then(d => {
        const ds_id = d._id;
        indicators.forEach( i => {
            //let countries = await getIndicatorCountries(i._id);
            delete i._id;
            i.dataset = ds_id;
            const new_ind = new Indicator(i);
            console.log(countries)
            new_ind.save()
        })

        res.redirect('/admin/datasets');
    }).catch(e => {
        throw e;
    })*/

    let d = await dataset.save();
    if (d) {
        const ds_id = d._id;
        for (const i of indicators) {
            let countries = await getIndicatorCountries(i._id);
            delete i._id;
            i.dataset = ds_id;
            const new_ind = new Indicator(i);
            countries = countries.map( c => c.country.value).join();
            new_ind.country = countries;
            new_ind.save()
        }
        res.redirect('/admin/datasets');
    } else {
        throw "Something went wrong"
    }
}

export const submitIndicator = async(req, res) => {

    const form = req.body;

    const ds_id = req.params.dataset_id

    form.indicator_related_themes = await fixTagsSemicolons(form.indicator_related_themes, 'themes');
    form.indicator_related_landvoc_concepts = await fixTagsSemicolons(form.indicator_related_landvoc_concepts, 'concepts');

    const ind = buildIndicator(form);

    const indicator = new Indicator(ind);

    indicator.dataset = ds_id;

    indicator.save().then(i => {
        res.redirect(`/admin/datasets/${ds_id}/indicators`);
    }).catch(e => {
        throw e;
    })
}

export const updateDataset = async(req, res) => {

    const form = req.body;

    let file = req.files.dataset_logo_file;
    let url = req.body.dataset_logo_url;
    let image = {};
    if (file) {
        image.data = file.data;
        image.contentType = file.mimetype;
    }
    if (url) {
        image.url = url;
    }

    form.updated_at = new Date(Date.now()).toISOString();

    // fix tags
    form.dataset_related_themes = await fixTagsSemicolons(form.dataset_related_themes, 'themes');
    form.dataset_related_landvoc_concepts = await fixTagsSemicolons(form.dataset_related_landvoc_concepts, 'concepts');
    let dataset = buildDataset(form);
    // logo if new
    if (file || url) {
        dataset.meta.dataset.logo = image;
    } else {
        delete dataset.meta.dataset.logo;
    }
    dataset = flat(dataset);

    Dataset.findOneAndUpdate({
        _id: form.id
    }, dataset).then(d => {
        res.redirect('/admin/datasets');
    }).catch(e => {
        throw e;
    })

}

export const updateIndicator = async(req, res) => {

    const form = req.body;

    const ds_id = req.params.dataset_id

    form.updated_at = new Date(Date.now()).toISOString();

    form.indicator_related_themes = await fixTagsSemicolons(form.indicator_related_themes, 'themes');
    form.indicator_related_landvoc_concepts = await fixTagsSemicolons(form.indicator_related_landvoc_concepts, 'concepts');

    const indicator = buildIndicator(form);

    Indicator.findOneAndUpdate({
        _id: form.id
    }, indicator).then(i => {
        res.redirect(`/admin/datasets/${i.dataset}/indicators`);
    }).catch(e => {
        throw e;
    })

}

export const deleteDataset = (req, res) => {

    Dataset.remove({_id: req.params.id}).then(d => {
        res.redirect('/admin/datasets');
    }).catch(e => {
        throw e;
    })

}

export const deleteIndicator = (req, res) => {

    Indicator.remove({_id: req.params.id}).then(d => {
        res.redirect('/admin/datasets');
    }).catch(e => {
        throw e;
    })

}

export const editStyle = async(req, res) => {
    const dataset_id = req.params.dataset_id;
    const indicator_id = req.params.indicator_id;
    const indicator = req.query.name;

    const style = await Style.findOne({indicator: indicator_id});

    res.render("../views/pages/new-style", {
        dataset_id, indicator_id, style, indicator
    });

}

export const updateStyle = (req, res) => {

    const dataset_id = req.params.dataset_id;
    const indicator_id = req.params.indicator_id;
    const type = req.body.type;

    const categorical = req.body.categorical;

    let newStyle = {
        indicator: indicator_id,
        type: type,
        properties: {
            palette: (type === 'graduated') ? req.body.palette : 'OrRd',
            color: (type === 'proportional') ? req.body.color: '#00FF00',
            shape: (type === 'proportional') ? req.body.shape: 'circle',
            radius: (type === 'heatmap') ? req.body.radius : 8,
            blur: (type === 'heatmap') ? req.body.blur : 15,
            opacity: req.body.opacity,
        },
        hide: (req.body.hide === "yes") ? true : false,
        categorical: categorical ? categorical : null,
        allowed: req.body.allowed,

    }

    Style.update({indicator: indicator_id}, newStyle , {upsert: true})
    .then( s => {

        res.json({
            ok: 1
        })
        //res.redirect(`/admin/datasets/${dataset_id}/indicators`)

    })
    .catch( e => {

        res.json({
            ok: 0,
            message: "An error ocurred.}"
        })

    });

}

// SOURCES

export const createNewSource = (req, res) => {
    return res.render("../views/pages/sources")
}

// SPATIAL

export const uploadLayer = async(req, res) => {

    const file = req.files.file;
    const name = req.body.name;

    if (!file) {
        return res.render("../views/pages/spatial", {
            error: 1,
            error_message: `No file chosen.`
        })
    }

    if (!name) {
        return res.render("../views/pages/spatial", {
            error: 1,
            error_message: `Name must contain at least 3 characters.`
        })
    }

    const precheck = await helpers.precheck(name);

    if (precheck) {

        if (!precheck.rows[0].exists) {

            const upload = await helpers.upload(file, name);

            if (upload.ok) {

                const publish = await helpers.publish(name);

                return res.render("../views/pages/spatial", {
                    error: 0,
                    error_message: null,
                    confirmation: "Layer has been published!"
                })

            } else {

                return res.render("../views/pages/spatial", {
                    error: 1,
                    error_message: upload.message,
                    confirmation: null,
                })

            }

        } else {

            return res.render("../views/pages/spatial", {
                error: 1,
                error_message: `Table '${name}' already exists in the database.`,
                confirmation: null,
            })

        }

    } else {

        return res.render("../views/pages/spatial", {
            error: 1,
            error_message: `Something is wrong with the server. Please try again in a few minutes.`,
            confirmation: null,
        })

    }

}

export const createLayer = (req, res) => {

    res.render("../views/pages/spatial", {
        error: 0,
        error_message: null,
        confirmation: null,
    })

}

export const addNonSpatial = (req, res) => {
    res.render('../views/pages/non-spatial', {
        countries: COUNTRIES_ISO,
        layer: null
    });
}

export const manageWmsStores = async(req, res) => {

    const tab = req.query.tab;

    const stores = await wms.getWmsStores();

    let template = (tab === 'new') ? '../views/pages/wmsstores-new' : '../views/pages/wmsstores-manage';

    res.render(template, {stores: stores.data});
}

export const errorOccured = (req, res) => {

    res.render("../views/pages/error")

}

export const browseLayers = async(req, res) => {

    const layers = await grabLayers();
    const geoserver = config.geoserver;

    res.render('../views/pages/layers', {
        layers, geoserver
    });

}

// Landvoc

export const landvocPage = async(req,res) => {

    const themes = await Theme.find({});
    const concepts = await Concept.find({});

    res.render("../views/pages/landvoc", {themes, concepts});
}

// HELPERS
const buildDataset = (form) => {

    const dataset = {};

    dataset.name = form.name;
    dataset.about = form.about;
    dataset.reference = form.reference;
    dataset.updated_at = form.updated_at;

    dataset.meta = {
        dataset: {
            label: form.dataset_label,
            description: form.dataset_description,
            id: form.dataset_id,
            logo: form.dataset_logo,
            license: form.dataset_license,
            copyright_details: form.dataset_copyright_details,
            organization: form.dataset_organization,
            organizationUri: form.dataset_organization_uri,
            related_overarching_categories: '',
            related_themes: form.dataset_related_themes || '',
            related_landvoc_concepts: form.dataset_related_landvoc_concepts || ''
        }
    };

    return dataset

}

const buildIndicator = (form) => {

    const indicator = {};

    indicator.name = form.name;
    indicator.about = form.about;
    indicator.type = form.type;
    indicator.dataset = form.dataset;
    indicator.region = form.region || '';
    indicator.country = form.country || '';
    indicator.updated_at = form.updated_at;

    switch (form.type) {

        case 'Vector':
            indicator.vector = {
                source_url: form.source_url,
                format: form.format,
                geometry: form.geometry,
                encoding: form.encoding
            };
            break;

        case 'Raster':
            indicator.raster = {
                source_url: form.source_url,
                format: form.format
            };
            break;

        case 'WMS':
            indicator.wms = {
                source_url: form.source_url
            };
            break;

        case 'SPARQL':
            indicator.sparql = {
                source_url: form.source_url
            };
            break;

        default:
            break;

    }

    indicator.meta = {
        indicator: {
            label: form.indicator_label,
            description: form.indicator_description,
            picture: form.indicator_picture,
            dataset: form.indicator_dataset,
            id: form.indicator_id,
            min: form.indicator_min || '',
            max: form.indicator_max || '',
            measurement_unit: form.indicator_measurement_unit || '',
            has_coded_value: form.indicator_has_coded_value || '',
            high_low: form.indicator_high_low || '',
            related_overarching_categories: '',
            related_themes: form.indicator_related_themes || '',
            related_landvoc_concepts: form.indicator_related_landvoc_concepts || ''
        }
    };

    return indicator

}
