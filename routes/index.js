import express from 'express';
import express_graphql from 'express-graphql';
import {graphqlUploadExpress} from 'graphql-upload';
import upload from 'express-fileupload';

import * as authentication from './authentication';
import * as usersSchema from '../schemas/users';
import * as views from './views';
import * as datasets from './datasets';
import * as nonspatial from './nonspatial';
import * as helpers from './helpers';
import * as files from './files';
import * as wms from './wms';
import * as compositions from './compositions';

const v1 = express.Router();
const v0 = express.Router();

v0.use(upload({useTempFiles: true, tempFileDir: '/tmp/'}));
v1.use(upload());

export default function(app, passport) {

    // REST

    //v1.route('/login')
    /**
     * @api {POST} /api/v1/login Sign in
     * @apiName Login
     * @apiGroup Authentication
     * @apiPermission none
     *
     * @apiParam {String} email Email address
     * @apiParam {String} password Password
     *
     * @apiSuccess (200) {String} token JWT
     * @apiError (401)
     */
    //.post(passport.authenticate('local-login'), authentication.login)

    //v1.route('/register')
    //.post(passport.authenticate('local-signup'), authentication.register)

    //v1.route('/isloggedin')
    //.get(authentication.isLoggedIn)

    v1.route('/datasets')
    /**
        * @api {GET} /api/v1/datasets Grab all the existing datasets
        * @apiName Viewer
        * @apiGroup Viewer
        * @apiPermission none
        *
        * @apiSuccess (200) {Array} Array of objects representing datasets (look: /models/dataset.js)
        * @apiError (401)
        */
        .get(datasets.getDatasets)
    /**
      *
      * Obsolete
      *
      */
        .post(datasets.findDatasets)

    v1.route('/find/datasets/theme')
    /**
    * @api {get} /api/v1/dataset/theme Grab all the datasets by theme name
    * @apiName Viewer
    * @apiGroup Viewer
    * @apiPermission none
    *
    * @apiParam {String} theme landvoc theme
    *
    * @apiSuccess (200) {Array} datasets an array of objects representing indicators (look: /models/indicator.js)
    * @apiError (401)
    */
        .post(datasets.getDatasetsByTheme)

    v1.route('/dataset/:id')
    /**
    * @api {get} /api/v1/dataset/:id Grab one dataset
    * @apiName Viewer
    * @apiGroup Viewer
    * @apiPermission none
    *
    * @apiParam {String} id a unique id (mongo _id or any other string)
    *
    * @apiSuccess (200) {Object} dataset object representing a single dataset (look: /models/dataset.js)
    * @apiError (401)
    */
        .get(datasets.getDatasetById)

    v1.route('/indicator/:id')
    /**
    * @api {get} /api/v1/indicator/:id Grab one indicator
    * @apiName Viewer
    * @apiGroup Viewer
    * @apiPermission none
    *
    * @apiParam {String} id a unique id (mongo _id or any other string)
    *
    * @apiSuccess (200) {Object} indicator object representing a single indicator (look: /models/indicator.js)
    * @apiError (401)
    */
        .get(datasets.getIndicatorById)

    v1.route('/indicators/dataset/:id')
    /**
    * @api {get} /api/v1/indicators/:id Grab all the indicators by dataset id
    * @apiName Viewer
    * @apiGroup Viewer
    * @apiPermission none
    *
    * @apiParam {String} id a unique id (mongo _id or any other string)
    *
    * @apiSuccess (200) {Array} datasets an array of objects representing indicators (look: /models/indicator.js)
    * @apiError (401)
    */
        .get(datasets.getIndicatorsByDataset)

    v1.route('/indicators/theme')
    /**
    * @api {get} /api/v1/indicators/theme Grab all the indicators by theme name
    * @apiName Viewer
    * @apiGroup Viewer
    * @apiPermission none
    *
    * @apiParam {String} theme landvoc theme
    *
    * @apiSuccess (200) {Array} datasets an array of objects representing indicators (look: /models/indicator.js)
    * @apiError (401)
    */
        .post(datasets.getIndicatorsByTheme)

    v1.route('/indicators/country')
    /**
    * @api {get} /api/v1/indicators/country Grab all the indicators by country
    * @apiName Viewer
    * @apiGroup Viewer
    * @apiPermission none
    *
    * @apiParam {String} country iso code
    *
    * @apiSuccess (200) {Array} datasets an array of objects representing indicators (look: /models/indicator.js)
    * @apiError (401)
    */
        .post(datasets.getIndicatorsByCountry)

    v1.route('/indicators/advanced')
    /**
    * @api {post} /api/v1/indicators/advanced Grab all the indicators by specified fields
    * @apiName Viewer
    * @apiGroup Viewer
    * @apiPermission none
    *
    * @apiParam {String} label (look: /models/indicator)
    * @apiParam {Boolean} type-Vector
    * @apiParam {Boolean} type-Raster
    * @apiParam {Boolean} type-WMS
    * @apiParam {String} description
    * @apiParam {String} measurement_unit
    * @apiParam {String} high_low
    * @apiParam {String} related_themes
    * @apiParam {String} related_concepts

    *
    * @apiParam {String} query
    *
    * @apiSuccess (200) {Array} datasets an array of objects representing indicators (look: /models/indicator.js)
    * @apiError (401)
    */
        .post(datasets.searchAdvanced)

    v1.route('/sparql/:indicator')
    /**
    * @api {get} /api/v1/sparql/:indicator Grab a sparql layer by id
    * @apiName Viewer
    * @apiGroup Viewer
    * @apiPermission none
    *
    * @apiParam {String} indicator a unique id
    *
    * @apiSuccess (200) {Object} indicator an object representing indicator (look: /models/indicator.js)
    * @apiError (401)
    */
        .get(datasets.getDataBySparql)

    v1.route('/test').get(datasets.parseQuery)

    v1.route('/example/dataset')
    /**
    *
    * Obsolete
    *
    */
        .get(datasets.getExampleDataset)

    v1.route('/themes')
    /**
    * @api {get} /api/v1/themes Grab existing themes
    * @apiName Viewer
    * @apiGroup Landvoc
    * @apiPermission none
    *
    *
    * @apiSuccess (200) {Array} themes
    * @apiError (401)
    */
        .get(helpers.grabThemes)

    v1.route('/concepts')
    /**
    * @api {get} /api/v1/concepts Grab existing concepts
    * @apiName Viewer
    * @apiGroup Landvoc
    * @apiPermission none
    *
    *
    * @apiSuccess (200) {Array} concepts
    * @apiError (401)
    */
        .get(helpers.grabConcepts)

    v0.route('/update/themes')
    /**
    * @api {get} /api/v1/update/themes Update existing themes using the sparql interface
    * @apiName Admin
    * @apiGroup Landvoc
    * @apiPermission none
    *
    *
    * @apiSuccess (200) {number} ok 0 or 1
    * @apiError (401)
    */
        .get(helpers.updateThemes)

    v0.route('/update/concepts')
    /**
    * @api {get} /api/v1/update/concepts Update existing concepts using the sparql interface
    * @apiName Admin
    * @apiGroup Landvoc
    * @apiPermission none
    *
    *
    * @apiSuccess (200) {number} ok 0 or 1
    * @apiError (401)
    */
        .get(views.isLoggedIn, helpers.updateConcepts)

    v1.route('/compositions/:id')
    /**
    * @api {get} /api/v1/compositions/:id Grab a MapComposition object
    * @apiName Viewer
    * @apiGroup Compositions
    * @apiPermission none
    *
    * @apiParam {String} id a unique id
    *
    * @apiSuccess (200) {Object} composition an object representing map composition (look: /models/compositions.js)
    * @apiError (401)
    */
        .get(compositions.getMapComposition)

    v1.route('/composition/new')
    /**
    * @api {get} /api/v1/composition/new Create a MapComposition object
    * @apiName Viewer
    * @apiGroup Compositions
    * @apiPermission none
    *
    * @apiParam {Array} indicators an array of styles assigned to indicators
    *
    * @apiSuccess (200) {Object} composition an object representing map composition (look: /models/compositions.js)
    * @apiError (401)
    */
        .post(compositions.createMapComposition)


    // ADMIN VIEWS

    v0.route('/')
    /**
    * @api {get} /admin Admin landing page
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission none
    *
    * @apiSuccess (200) {Html} /views/pages/login.ejs
    * @apiError (404)
    */
        .get(views.login).post(passport.authenticate('local-login-admin'), views.authorization)

    v0.route('/logout')
    /**
    * @api {get} /admin Admin landing page
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission none
    *
    * @apiSuccess (200) {Html} /views/pages/login.ejs
    * @apiError (404)
    */
        .get(views.logOut)

    v0.route('/settings')
    /**
    * @api {get} /admin/settings Admin settings
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/settings.ejs
    * @apiError (404)
    */
        .get(views.isLoggedIn, views.settings).post(views.isLoggedIn, authentication.changePassword)

    v0.route('/datasets')
    /**
    * @api {get} /admin/datasets Datasets table
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String} tab a query parameter 'mongo' or 'drupal'
    *
    * @apiSuccess (200) {Html} /views/pages/datasets-*.ejs
    * @apiError (404)
    */
        .get(views.isLoggedIn, views.datasets)

    v0.route('/indicators')
    /**
    * @api {get} /admin/indicators Indicators overview table
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/indicators-overview.ejs
    * @apiError (404)
    */
        .get(views.isLoggedIn, views.indicatorsOverview)

    v0.route('/browse/layers')
    /**
    * @api {get} /admin/browse/layers Layers overview
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/layers.ejs
    * @apiError (404)
    */
        .get(views.isLoggedIn, views.browseLayers)

    v0.route('/dataset/:id')
    /**
    * @api {get} /admin/dataset/:id Edit dataset
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String} id a unique id
    * @apiParam {Object} form a form representing a dataset (look: /models/dataset.js)
    *
    * @apiSuccess (200) {Html} /views/pages/edit-dataset.ejs
    * @apiError (404)
    */
        .get(views.isLoggedIn, views.editDataset)

    v0.route('/datasets/add')
    /**
    * @api {get} /admin/datasets/add Create dataset
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/new-dataset.ejs
    * @apiError (404)
    */
        .get(views.isLoggedIn, views.newDataset)
    /**
    * @api {post} /admin/datasets/add Create dataset
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {Object} form a form representing a dataset (look: /models/dataset.js)
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
        .post(views.isLoggedIn, views.submitDataset)

    v0.route('/datasets/update')
    /**
    * @api {post} /admin/datasets/add Update dataset
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {Object} form a form representing a dataset (look: /models/dataset.js)
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
        .post(views.isLoggedIn, views.updateDataset)

    v0.route('/datasets/delete/:id')
    /**
    * @api {get} /admin/datasets/delete/:id Remove dataset
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String} id a unique id
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
        .get(views.isLoggedIn, views.deleteDataset)

    v0.route('/datasets/:dataset_id/indicators')
    /**
    * @api {get} /admin/datasets/:dataset_id/indicators Indicators by dataset id table
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String} dataset_id a unique id
    *
    * @apiSuccess (200) {Html} /views/pages/indicators-*.ejs
    * @apiError (404)
    */
        .get(views.isLoggedIn, views.indicators)

    v0.route('/datasets/:dataset_id/indicators/add')
    /**
    * @api {get} /admin/dataset/:dataset_id/indicators/add Create indicator
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String} dataset_id a unique id
    *
    * @apiSuccess (200) {Html} /views/pages/new-indicator.ejs
    * @apiError (404)
    */
        .get(views.isLoggedIn, views.newIndicator).post(views.isLoggedIn, views.submitIndicator)

    v0.route('/datasets/:dataset_id/indicator/:indicator_id')
    /**
    * @api {get} /admin/datasets/:dataset_id/indicator/:indicator_id Edit indicator
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  dataset_id a unique id
    * @apiParam {String}  indicator_id a unique id
    *
    * @apiSuccess (200) {Html} /views/pages/edit-indicator.ejs
    * @apiError (404)
    */
    .get(views.isLoggedIn, views.editIndicator)

    v0.route('/indicator/update')
    /**
    * @api {get} /admin/indicator/update Edit indicator
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {Object}  form an object representing an indicator (look: /models/indicator.js)
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
    .post(views.isLoggedIn, views.updateIndicator)

    v0.route('/indicators/delete/:id')
    /**
    * @api {get} /admin/indicators/delete/:id Remove indicator
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  id a unique id
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
    .get(views.isLoggedIn, views.deleteIndicator)

    v0.route('/indicators/hide/:id')
    /**
    * @api {get} /admin/indicators/delete/:id Remove indicator
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  id a unique id
    * @apiParam {String}  hide yes or no
    *
    * @apiSuccess (200) {Boolean}
    * @apiError (404)
    */
    .post(views.isLoggedIn, views.hideIndicator)

    v0.route('/datasets/:dataset_id/indicator/:indicator_id/style')
    /**
    * @api {get} /admin/datasets/:dataset_id/indicator/:indicator_id/style Edit style
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  dataset_id a unique id
    * @apiParam {String}  indicator_id a unique id
    *
    * @apiSuccess (200) {Html} /views/pages/new-style.ejs
    * @apiError (404)
    */
    .get(views.isLoggedIn, views.editStyle)
    /**
    * @api {post} /admin/datasets/:dataset_id/indicator/:indicator_id/style Update style
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  dataset_id a unique id
    * @apiParam {String}  indicator_id a unique id
    * @apiParam {Object}  form an object representing a style (look: /models/style.js)
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
    .post(views.isLoggedIn, views.updateStyle)

    v0.route('/sources')
    /**
    * @api {get} /sources Data sources overview
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/sources.ejs
    * @apiError (404)
    */
    .get(views.isLoggedIn, views.createNewSource)

    v0.route('/create/layer')
    /**
    * @api {get} /admin/create/layer Create layer
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/spatial.ejs
    * @apiError (404)
    */
    .get(views.isLoggedIn, views.createLayer)
    /**
    * @api {post} /admin/create/layer Create layer
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  name
    * @apiParam {File}  file supported formats: shp, geojson, kml, gml
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
    .post(views.isLoggedIn, views.uploadLayer)

    v0.route('/webservices')
    /**
    * @api {get} /admin/webservices Webservices management
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/wmsstores.ejs
    * @apiError (404)
    */
    .get(views.isLoggedIn, views.manageWmsStores)

    v0.route('/landvoc')
    /**
    * @api {get} /admin/landvoc themes and concepts
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/landvoc.ejs
    * @apiError (404)
    */
    .get(views.isLoggedIn, views.landvocPage)

    v0.route('/webservices/layers/:store')
    /**
    * @api {get} /admin/webservices/layers/:store Get layers by store id
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  store a unique store name
    *
    * @apiSuccess (200) {Array} published an array representing geoserver published layers
    * @apiSuccess (200) {Array} available an array representing geoserver unpublished layers
    * @apiError (404)
    */
    .get(views.isLoggedIn, wms.getStoreData)

    v0.route('/webservices/publish')
    /**
    * @api {get} /admin/webservices/publish/:store/:layer Publish an unpublished layer
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  store a unique store name
    * @apiParam {String}  layer a layer name
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
    .post(views.isLoggedIn, wms.publishWmsLayer)

    v0.route('/webservices/unpublish/:store/:layer')
    /**
    * @api {get} /admin/webservices/unpublish/:store/:layer unublish an unpublished layer
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  store a unique store name
    * @apiParam {String}  layer a layer name
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
    .get(views.isLoggedIn, wms.unpublishWmsLayer)

    v0.route('/webservices/store')
    /**
    * @api {post} /admin/webservices/store Create store
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  name a unique store name
    * @apiParam {String}  capabilities getCapabilities url
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
    .post(views.isLoggedIn, wms.createWmsStore)

    v0.route('/webservices/store/delete/:name')
    /**
    * @api {post} /admin/webservices/store Create store
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String}  name a unique store name
    *
    * @apiSuccess (200) {Redirect}
    * @apiError (404)
    */
    .get(views.isLoggedIn, wms.deleteWmsStore)


    //v0.route('/upload') // Changed to function
    //.post(files.upload) // #TODO: authenticate user

    v0.route('/nonspatial')
    /**
    * @api {get} /admin/nonspatial Add non-spatial layer form
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/non-spatial.ejs
    * @apiError (404)
    */
    .get(views.isLoggedIn, views.addNonSpatial);

    v1.route('/nonspatial')
    /**
    * @api {get} /api/v1/nonspatial Create layer from csv
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * TODO
    *
    * @apiSuccess (200)
    * @apiError (404)
    */
    .post(views.isLoggedIn, nonspatial.createLayerFromCSV);

    v0.route('/error')
    /**
    * @api {get} /admin/error Something went wrong...
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Html} /views/pages/error.ejs
    * @apiError (404)
    */
    .get(views.isLoggedIn, views.errorOccured);

    v0.route('/layers')
    /**
    * @api {get} /admin/layers Add non-spatial layer form
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiSuccess (200) {Array} layers an array of published layers from Geoserver
    * @apiError (404)
    */
    .get(helpers.getLayers);

    v0.route('/layers/:layer')
    /**
    * @api {get} /admin/nonspatial Add non-spatial layer form
    * @apiName Admin
    * @apiGroup Views
    * @apiPermission admin
    *
    * @apiParam {String} layer layer name
    *
    * @apiSuccess (200) {String} url geoserver geoserver url
    * @apiSuccess (200) {Number} ok 1 or 0
    * @apiError (404)
    */
    .get(helpers.getLayerInfo);

    app.use('/api/v1', v1);
    app.use('/admin', v0);

}
