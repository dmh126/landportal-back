import Dataset from '../models/dataset';
import Indicator from '../models/indicator';
import Style from '../models/style';
import Theme from '../models/theme';
import {
    getAllDatasetsBySparql,
    getIndicatorsBySparql,
    getOneIndicatorBySparql,
    getOneDatasetBySparql,
    getIndicatorsAdvanced,
    getIndicatorCountries,
    getThemes,
    getConcepts
} from './sparql.js';
import {indicatorById} from '../constants/SparqlQuery';
import {THEMES} from '../constants/LandVoc';
import {REGIONS_COUNTRIES, REGIONS} from '../constants/countries';
import {checkCountries} from './helpers.js'
import axios from 'axios';
const SparqlGenerator = require('sparqljs').Generator;
const SparqlParser = require('sparqljs').Parser;
const _ = require('lodash');

export const getDatasets = async (req, res) => {

    try {

        let result = await Dataset.find({}).sort({name: 1});
        res.status(200).json(result);

    } catch (e) {

        res.status(500).json([]);

    }

}

export const findDatasets = (req, res) => {
    // name
    const name = req.body.name || '';
    // dates
    const created_at_from = req.body.created_at_from || '';
    const created_at_to = req.body.created_at_to || '';
    let created_at;
    if (created_at_to && created_at_from) {
        created_at = {
            $gte: new Date(created_at_from),
            $lte: new Date(created_at_to)
        }
    }

    // query
    const query = {
        name: {
            $regex: name,
            $options: 'i'
        }
    }
    if (created_at)
        query.created_at = created_at;

    Dataset.find(query).then(d => {
        res.json(d)
    }).catch(e => {
        throw e;
    })
}

export const getDatasetById = async(req, res) => {

    const id = req.params.id;

    if (id.match(/^[0-9a-fA-F]{24}$/)) {

        try {

            let ds = await Dataset.findOne({_id: id});

            if (ds) {
                res.status(200).json({ok: 1, dataset: ds});
            } else {
                res.status(200).json({ok: 0, dataset: null});
            }

        } catch(e) {

            res.status(500).json({ok: 0, dataset: null});

        }
    } else {

        try {

            let ds = await getOneDatasetBySparql(id);

            ds = buildDatasetsList(ds);

            if (ds.length) {
                res.status(200).json({ok: 1, dataset: ds[0]});
            } else {
                 res.status(200).json({ok: 0, dataset: null});
            };


        } catch(e) {

            res.status(500).json({ok: 0, dataset: null});

        };


    }

}

export const getIndicatorsByDataset = async(req, res) => {

    const id = req.params.id;
    const {reference, search} = req.query;

    //if (id.match(/^[0-9a-fA-F]{24}$/)) {
    const mongoQuery = {
        dataset: id,
        "$or": [{hidden: false}, {hidden: {"$exists": false}}]
    };


    if (search) {
        mongoQuery['meta.indicator.related_themes'] = { "$regex": `.*${search}.*` };
    };

    try {

        let ind = await Indicator.find(mongoQuery).populate('dataset', 'name meta.dataset.organization');

        res.status(200).json(ind);

    } catch(e) {

        res.status(500).json([]);

    };

    /*
    } else {
        // deprecated
        let sparql = await getIndicatorsBySparql(id);

        sparql = sparql.map(item => {

            return {
                name: item.label.value,
                type: "SPARQL",
                sparql: {
                    source_url: item.id.value,
                },
                _id: item.id.value
            }

        });

        let mongo = [];

        if (reference && reference.match(/^[0-9a-fA-F]{24}$/)) {
            mongo = await Indicator.find({dataset: reference})
        }


        const result = sparql.concat(mongo);

        res.json(result);

    }*/

}

export const getIndicatorsByTheme = async(req, res) => {

    const theme = req.body.theme;
    try {

        let ind = await Indicator.find({
            'meta.indicator.related_themes': { "$regex": `.*${theme}.*` }
        }).populate('dataset', 'name meta.dataset.organization');

        res.status(200).json(ind);

    } catch(e) {

        res.status(500).json([]);

    }

}

export const getDatasetsByTheme = async(req, res) => {

    const theme = req.body.theme;
    try {

        let ds = await Dataset.find({
            'meta.dataset.related_themes': { "$regex": `.*${theme}.*` }
        });

        res.status(200).json(ds);

    } catch(e) {

        res.status(500).json([]);

    }

}

export const getIndicatorsByCountry = async(req, res) => {

    const country = req.body.country;
    /*
    let region;
    let countries_regex;
    if (/^[0-9]*$/.test(country)) {
        region = country;

        let countries_list = REGIONS.find(i => region.includes(i.code));
        if (countries_list.countries) {
            countries_regex = `(`;
            countries_list.countries.forEach( (c, index) => {
                if (index != 0) {
                    countries_regex += `${c.code}`;
                } else {
                    countries_regex += `|${c.code}`;
                }
            });
            countries_regex += `)`;
        }

    } else {
        countries_regex = `.*${country}.*`
    }*/

    try {

        let ind = await Indicator.find({
            '$or' : [
                {region: { "$regex": `.*${country}.*`, "$options": "i" }},
                {country: { "$regex": `.*${country}.*`, "$options": "i" }}
            ]
        }).populate('dataset', 'name meta.dataset.organization').limit(50);

        let dsIds = Array.from(new Set(ind.map(i => i.dataset._id)));

        let ds = await Dataset.find({_id: {'$in': dsIds}});

        res.status(200).json({
            indicators: ind,
            datasets: ds
        });

    } catch(e) {

        res.status(500).json({});

    }

}

export const getIndicatorById = async(req, res) => {

    const id = req.params.id;

    const style = await Style.findOne({indicator: id});

    //if (id.match(/^[0-9a-fA-F]{24}$/)) {

        try {

            let ind = await Indicator.findOne({_id: id}).populate('dataset', 'name meta.dataset.organization meta.dataset.organizationUri meta.dataset.license');


            if (ind) {
                res.status(200).json({ok: 1, indicator: ind, style})
            } else {
                res.status(200).json({ok: 0, indicator: null})
            };

        } catch(e) {

            res.status(500).json({ok: 0, indicator: null})

        };

    /*
    } else {

        try {
            let ind = await getOneIndicatorBySparql(id);

            if (ind.length) {

                const result = buildIndicatorsList(ind)[0];

                res.status(200).json({ok: 1, indicator: result, style})

            } else {

                res.status(200).json({ok: 0, indicator: null});

            };

        } catch(e) {

            res.status(500).json({ok: 0, indicator: null});

        }

    }*/

}


// SPARQL

export const getDataBySparql = (req, res) => {

  const indicator = req.params.indicator;
  const format = req.query.format || "json";
  const mode = req.query.mode;

  let generator = new SparqlGenerator();

  let newQuery = _.cloneDeep(indicatorById);

  newQuery.where[2].values.push({ "?bindicator": "http://data.landportal.info/indicator/" + indicator });
  const queryString = generator.stringify(newQuery);
  const uri = 'https://landportal.org/sparql?query=';

  axios.get(`${uri}${encodeURIComponent(queryString)}&should-sponge=&timeout=0&debug=on&format=${format}`)
  .then( (resp) => {

      if (mode === 'download') {
          const ct = (format === 'xml') ? 'application/xml' : 'application/octet-stream';
          res.set('Content-Type', ct);
          res.send(resp.data);
      } else {
          res.json(resp.data);
      }

  })
  .catch( e => {
      reject(e)
  });

}


export const parseQuery = (req, res) => {

    var parser = new SparqlParser();
    var generator = new SparqlGenerator();
    var parsedQuery = parser.parse(
        `PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT *
FROM <http://landvoc.landportal.info>
WHERE {
?concept a skos:Concept ;
  skos:prefLabel ?label .
  FILTER (lang(?label) = 'en')
}`
    )

    res.json(parsedQuery);

}

/*
    Utility function. Grabs a value from a sparql entry
    @param _object {value: String, ...}
    @param name String

    @return value String

*/
const getValue = (_object, name) => {

    const field = _object[name];

    if (field) {

        return field.value || '';

    } else {

        return '';

    }

}

/*
    Utility function. Builds a consistent indicator (see: models/indicator.js)
    @param _object Object retrieved from sparql query
    @return indicator Object valid indicator

*/
const buildIndicatorFromSparql = (_object) => {

    const indicator = {
        _id: getValue(_object, 'id'),
        name: getValue(_object, 'label'),
        about: getValue(_object, 'description'),
        type: "SPARQL",
        sparql: {
            source_url: getValue(_object, 'id'),
        },

        meta: {
            indicator: {
                label: getValue(_object, 'label'),
                description: getValue(_object, 'description'),
                id: getValue(_object, 'id'),
                dataset: getValue(_object, 'dataset'),
                organization: getValue(_object, 'organization'),
                measurement_unit: getValue(_object, 'unit'),
                related_overarching_categories: getValue(_object, 'related_overarching_categories'),
                related_themes: getValue(_object, 'related_themes'),
                related_landvoc_concepts: getValue(_object, 'related_landvoc_concepts'),

                picture: getValue(_object, 'picture'),

                datasetId: getValue(_object, 'datasetId'),
                organizationId: getValue(_object, 'organizationId'),
            }
        }

    };

    return indicator;

}

/*

    Same but dataset

*/
const buildDatasetFromSparql = (_object) => {

    const dataset = {
        _id: getValue(_object, 'id'),
        id: getValue(_object, 'id'),
        name: getValue(_object, 'label'),
        about: getValue(_object, 'description'),

        meta: {
            dataset: {
                label: getValue(_object, 'label'),
                description: getValue(_object, 'description'),
                id: getValue(_object, 'id'),
                license: getValue(_object, 'license'),
                logo: getValue(_object, 'logo'),
                organization: getValue(_object, 'organization'),
                organizationUri: getValue(_object, 'pubURI'),
                related_overarching_categories: getValue(_object, 'related_overarching_categories'),
                related_themes: getValue(_object, 'related_themes'),
                related_landvoc_concepts: getValue(_object, 'related_landvoc_concepts'),
            }
        }

    };

    return dataset;

}

/*
    Utility function. Aggregates all the entries for the given indicator by id.
    Creates landvoc entries depending on the URI pattern.
    @param arr Array an array of objects from sparql query
    @return Array list of indicators

*/
export const buildIndicatorsList = (arr) => {

    return _(arr).groupBy('id.value')
        .map( function(value, key) {
             const indicator = value[0];
             indicator.related_overarching_categories = {value: ""};
             indicator.related_themes = {value: ""};
             indicator.related_landvoc_concepts = {value: ""};

             value.forEach( entry => {

                 var type = entry.landvocURI.value;

                 if (type.includes("landvoc/overarching")) {
                     indicator.related_overarching_categories.value += entry.landvoc.value + ';';
                     indicator.related_overarching_categories.type = 'literal';
                 } else if (type.includes("landvoc/theme")) {
                     indicator.related_themes.value += entry.landvoc.value + ';';
                     indicator.related_themes.type = 'literal';
                 } else {
                     indicator.related_landvoc_concepts.value += entry.landvoc.value + ';';
                     indicator.related_landvoc_concepts.type = 'literal';
                 }
             });

             return buildIndicatorFromSparql(indicator);

        }).value();

}

/*

    Same but datasets

*/
export const buildDatasetsList = (arr) => {

    return _(arr).groupBy('id.value')
        .map( function(value, key) {
             const ds = value[0];
             ds.related_overarching_categories = {value: ""};
             ds.related_themes = {value: ""};
             ds.related_landvoc_concepts = {value: ""};

             value.forEach( entry => {
                 if ( entry.landvoc) {
                     var type = entry.landvocURI.value;

                     if (type.includes("landvoc/overarching")) {
                         ds.related_overarching_categories.value += entry.landvoc.value + ';';
                         ds.related_overarching_categories.type = 'literal';
                     } else if (type.includes("landvoc/theme")) {
                         ds.related_themes.value += entry.landvoc.value + ';';
                         ds.related_themes.type = 'literal';
                     } else {
                         ds.related_landvoc_concepts.value += entry.landvoc.value + ';';
                         ds.related_landvoc_concepts.type = 'literal';
                     }
                 }
             });

             return buildDatasetFromSparql(ds);

        }).value();

}


/*
    Grabs 6 completely random indicators.

    DEPRECATED.
*/
export const getRandomIndicators = (req, res) => {

    Indicator.aggregate([{ $sample: {size: 6 }}])
    .then( indicators => {
        res.json(indicators)
    })
    .catch( err => {
        if (err) throw err;
    })

}

export const getExampleDataset = async(req, res) => {

    try {
        const ds = await Dataset.findOne({});
        const ind = await Indicator.findOne({});
        res.status(200).json({
            datasets: [ds],
            indicators: [ind],
        })
    } catch(e) {
        res.status(500).json({})
    }

}


/*

    Advanced search
    Grab and filter indicators from mongo and sparql.

*/

export const searchIndicatorsAdvanced = async(req, res) => {

    const body = req.body;

    let types = [];
    if (body['type-Vector']) {
        types.push('Vector');
        types.push('SPARQL'); // remove later
    }
    if (body['type-Raster']) {
        types.push('Raster');
    }
    if (body['type-WMS']) {
        types.push('WMS');
    }

    let region;

    switch (body.region) {

        case '001':
            region = ''
            break;
        case '002':
            region = ['002', '015', '202', '011', '014', '017', '018'];
            break;
        case '019':
            region = ['019', '419', '029', '013', '005', '003', '021'];
            break;
        case '142':
            region = ['142', '143', '030', '034', '035', '145'];
            break;
        case '150':
            region = ['150', '039', '151', '154', '155'];
            break;
        case '009':
            region = ['009', '053', '054', '057', '061'];
            break;
        default:
            region = body.region.split(',');

    }

    let country = body.country;

    if (body.minlat && body.minlon && body.maxlat && body.maxlon) {

        const bbox = {
            min: {
                lat: body.minlat,
                lon: body.minlon,
            },
            max: {
                lat: body.maxlat,
                lon: body.maxlon,
            }
        };

        country = checkCountries(bbox);

    }


    const mongo_query = {

        name: { "$regex": `.*${body.label}.*` },
        type: { "$in": types },

        region: (typeof region === 'string') ? {"$regex": `.*${region}.*`} : {"$in": region},
        country: (typeof country === 'string') ? {"$regex": `.*${country}.*`} : {"$in": country},

        //'meta.indicator.label': { "$regex": `.*${body.label}.*` },
        'meta.indicator.description': { "$regex": `.*${body.description}.*` },
        'meta.indicator.measurement_unit': { "$regex": `.*${escape(body.measurement_unit)}.*` },
        'meta.indicator.high_low': { "$regex": `.*${body.high_low}.*` },
        'meta.indicator.related_themes': { "$regex": `.*${body.related_themes}.*` },
        'meta.indicator.related_landvoc_concepts': { "$regex": `.*${body.related_concepts}.*` },

    }
    // grab max 20 indicators from both sources
    // grab sparql only if type is vector, region is global and no country selected
    const grab_sparql = types.includes('Vector') && '001'.includes(body.region) && country === '';
    let mongo_limit;
    if (grab_sparql) {
        mongo_limit = 10;
    } else {
        mongo_limit = 20;
    };
    const mongo_results = await Indicator.find(mongo_query).populate('dataset', 'name').limit(mongo_limit);

    if (grab_sparql) {

        const n_sparql = 20 - mongo_results.length;
        const indicators = await getIndicatorsAdvanced();
        const sparql_results = buildIndicatorsList(indicators)
        .filter( indicator => {

            let meta = indicator.meta.indicator;
            let condition = (
                meta.label.includes(body.label) && meta.description.includes(body.description) && meta.measurement_unit.includes(body.measurement_unit) && meta.organization.includes(body.organization) &&
                meta.related_themes.includes(body.related_themes) && meta.related_landvoc_concepts.includes(body.related_concepts)
            );

            if (condition) {
                return true;
            } else {
                return false;
            }

        });

        const combined = mongo_results.concat(sparql_results.slice(0, n_sparql));

        res.json(combined);

    } else {

        res.json(mongo_results);

    }

}

export const searchAdvanced = async(req,res) => {

    const query = req.body.query;

    if (!query) {

        return res.status(200).json({
            ok: 0,
            datasets: [],
            indicators: [],
            themes: [],
            countries: [],
        });

    };

    let keywords = query.split(/[\s,;!/+&.'"-]+/); // comment to fix editor '

    keywords = keywords.filter( k => k.length > 3);

    let indicators_or_list = [];
    let datasets_or_list = [];

    let keywords_regex = `^`;
    keywords.forEach( k => {
        keywords_regex += `(?=.*${k})`
    });
    keywords_regex += `.*$`;

    let countries = REGIONS_COUNTRIES.filter( r => {
        if (
            keywords.some(
                function(v) {
                    if (r.region) {
                        return r.region.toLowerCase().indexOf(v.toLowerCase()) >= 0;
                    } else if (r.country) {
                        return r.country.toLowerCase().indexOf(v.toLowerCase()) >= 0;
                    }
                })
            )
        {
            return true;
        } else {
            return false;
        }
    });

    let countries_regex = `^`;
    countries.forEach( c => {
        countries_regex += `(?=.*${c.code})`
    });
    countries_regex += `.*$`;

    indicators_or_list.push({'meta.indicator.label': { "$regex": keywords_regex, "$options": "i" } });
    indicators_or_list.push({'meta.indicator.description': { "$regex": keywords_regex, "$options": "i" } });
    indicators_or_list.push({'meta.indicator.related_themes': { "$regex": keywords_regex, "$options": "i" } });
    indicators_or_list.push({'meta.indicator.related_landvoc_concepts': { "$regex": keywords_regex, "$options": "i" } });
    if (countries.length) {
        indicators_or_list.push({'country': {"$regex": countries_regex, "$options": "i"}});
    }

    datasets_or_list.push({'meta.dataset.label': { "$regex": keywords_regex, "$options": "i" } });
    datasets_or_list.push({'meta.dataset.description': { "$regex": keywords_regex, "$options": "i" } });
    datasets_or_list.push({'meta.dataset.organization': { "$regex": keywords_regex, "$options": "i" } });
    datasets_or_list.push({'meta.dataset.related_themes': { "$regex": keywords_regex, "$options": "i" } });
    datasets_or_list.push({'meta.dataset.related_landvoc_concepts': { "$regex": keywords_regex, "$options": "i" } });

    const indicators_mongo_query = {
        '$or' : indicators_or_list
    }

    const datasets_mongo_query = {
        '$or': datasets_or_list
    }

    const indicators = await Indicator.find(indicators_mongo_query).sort({name: 1}).populate('dataset', 'name meta.dataset.organization');
    const datasets = await Dataset.find(datasets_mongo_query).sort({name: 1});
    let themes = await Theme.find({label: { "$regex": keywords_regex, "$options": "i" } });
    themes = themes.map(t => t.label).sort();

    res.status(200).json({
        ok: 1,
        indicators,
        datasets
    })
}
