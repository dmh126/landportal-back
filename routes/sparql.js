import _ from 'lodash';
import axios from 'axios';
import {
    indicatorById,
    indicatorMetaById,
    datasetMetaById,
    allDatasets,
    indicatorsByDataset,
    indicatorsAdvanced,
    countriesByIndicator,
    currentThemes, currentConcepts
} from '../constants/SparqlQuery';

const SparqlGenerator = require('sparqljs').Generator;
const SparqlParser = require('sparqljs').Parser;
/*
export const getDataBySparql = (req, res) => {

  const indicator = req.params.indicator;
  const format = req.query.format || "json";
  const mode = req.query.mode;

  let generator = new SparqlGenerator();

  let newQuery = _.cloneDeep(indicatorById);

  newQuery.where[2].values.push({ "?bindicator": "http://data.landportal.info/indicator/" + indicator });
  const queryString = generator.stringify(newQuery);
  const uri = 'https://landportal.org/sparql?query=';

  axios.get(`${uri}${encodeURIComponent(queryString)}&should-sponge=&timeout=0&debug=on&format=${format}`)
  .then( (resp) => {

      if (mode === 'download') {
          const ct = (format === 'xml') ? 'application/xml' : 'application/octet-stream';
          res.set('Content-Type', ct);
          res.send(resp.data);
      } else {
          res.json(resp.data);
      }

  })
  .catch( e => {
      reject(e)
  });

}*/

const defaultSparqlQuery = (queryObject) => {
    const generator = new SparqlGenerator();
    const newQuery = _.cloneDeep(queryObject);
    const queryString = generator.stringify(newQuery);
    const uri = `https://landportal.org/sparql?query=${encodeURIComponent(queryString)}&should-sponge=&timeout=0&debug=on&format=json`;
    return new Promise( (resolve, reject) => {
        axios.get(uri)
        .then( (response) => {
            resolve(response.data.results.bindings);
        })
        .catch( err => {
            reject(err)
        });
    });

}

export const getAllDatasetsBySparql = () => {

    return defaultSparqlQuery(allDatasets);

}

export const getIndicatorsBySparql = (id) => {

    const queryObject = indicatorsByDataset(id);

    return defaultSparqlQuery(queryObject);

}

export const getOneIndicatorBySparql = (id) => {

    const queryObject = indicatorMetaById(id);

    return defaultSparqlQuery(queryObject);

}

export const getOneDatasetBySparql = (id) => {

    const queryObject = datasetMetaById(id);

    return defaultSparqlQuery(queryObject);

}

export const getIndicatorsAdvanced = (id) => {

    const queryObject = indicatorsAdvanced();

    return defaultSparqlQuery(queryObject);

}

export const getIndicatorCountries= (id) => {

    const queryObject = countriesByIndicator(id);

    return defaultSparqlQuery(queryObject);

}

export const getThemes = () => {

    const queryObject = currentThemes();

    return defaultSparqlQuery(queryObject);

}

export const getConcepts = () => {

    const queryObject = currentConcepts();

    return defaultSparqlQuery(queryObject);

}

// testing
export const parseQuery = (req, res) => {

    var parser = new SparqlParser();
    var generator = new SparqlGenerator();
    var parsedQuery = parser.parse(
        `PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT *
FROM <http://landvoc.landportal.info>
WHERE {
?concept a skos:Concept ;
  skos:prefLabel ?label .
  FILTER (lang(?label) = 'en')
}`
    )

    res.json(parsedQuery);

}
