import User from '../models/user';

export const changePassword = (req, res) => {

  const old_password = req.body.old_password;
  const new_password = req.body.new_password;
  const retype_password = req.body.retype_password;
  const user_email = req.user.email;

  if (!(new_password === retype_password)) {

    return res.render(
      '../views/pages/settings',
      {
        error: true,
        message: "Passwords don't match.",
      }
    );
  }

  User.findOne({
    'email': user_email
  }).then( user => {

    if (!user) {

      return res.render(
        '../views/pages/settings',
        {
          error: true,
          message: "User not found.",
        }
      );

    }
    if (!user.validPassword(old_password)) {

      return res.render(
        '../views/pages/settings',
        {
          error: true,
          message: "Invalid password.",
        }
      );

    }

    user.password = user.generateHash(new_password);

    user.save( err => {

      if (err) throw err;

      return res.render(
        '../views/pages/settings',
        {
          error: false,
          message: "Password changed.",
        }
      );

    })


  })

}


export const login = (req, res) => {
  res.json(req.user.id);
}

export const register = (req, res) => {
  res.json(req.user.id);
}

export const isLoggedIn = (req, res) => {
  if (req.isAuthenticated()) {
    res.json(true)
  } else {
    res.json(false)
  }
}
