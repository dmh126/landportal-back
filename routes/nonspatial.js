import DataFrame from "dataframe-js";
import CSV from 'comma-separated-values';
import axios from 'axios';
import {COUNTRIES, LAYER_URL} from '../constants/countries.js';

export const createLayerFromCSV = (req, res) => {

    const file = req.files.file;
    const hasc = req.body.hasc || false;
    const group = req.body.group;
    const attribute = req.body.attribute;
    const stat = req.body.stat || "mean";
    let country = req.body.country || "country_bfa";

    const subdivision = parseInt(req.body.subdivision);
    if (subdivision === 2) {
        let s = country.split('_');

        country = s[0] + '_' + s[1].toUpperCase() + '_2';
    };

    if (file) {

        const csv = new CSV(file.data.toString(), {header: true}).parse();

        const df = new DataFrame(csv)

        const _obj = {};
        df.groupBy(group).aggregate( (g, n) => {
            _obj[ n[group] ] = getStats(g, attribute, stat);
        })
        console.log(country)
        axios.get(LAYER_URL(country))
        .then( layer => {
            console.log(layer.data)
            const geojson = addProperties(layer.data, _obj, subdivision);

            return res.render('../views/pages/non-spatial', {
                countries: COUNTRIES,
                layer: JSON.stringify(geojson),
            });
        })
        .catch( e => {
            console.log(e)
            return res.redirect('/admin/error');
        })

    } else {

        return res.json({ok: 0})

    }


}

const getStats = (group, attribute, method) => {

    let value;

    switch(method) {

        case "mean":
            value = group.stat.mean(attribute);
            break;
        case "min":
            value = group.stat.min(attribute);
            break;
        case "max":
            value = group.stat.max(attribute);
            break;
        case "sum":
            value = group.stat.sum(attribute);
            break;
        default:
            value = group.stat.mean(attribute);
            break;
    }

    return value;
}

const addProperties = (geojson, properties, subdivision) => {

    geojson.features.map( feature => {

        const adm_unit = feature.properties[`name_${subdivision}`];
        const hasc_code = feature.properties[`hasc_${subdivision}`];

        const properties_keys = Object.keys(properties);
        const adm_unit_1 = properties_keys.find( p => p.includes(adm_unit) || adm_unit.includes(p));

        feature.properties["indicator"] = properties[adm_unit] || properties[adm_unit_1] || properties[hasc_code] ||  null; // todo includes name instead of comparing strings

    })

    return geojson;
}
