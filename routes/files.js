import fs from 'fs';
import rimraf from 'rimraf';
import extract from 'extract-zip';
import ogr2ogr from 'ogr2ogr';
import axios from 'axios';
import glob from 'glob';
const { Pool } = require('pg')


import config from '../config/config';

export const upload = (file, name) => {
    const postgis = config.postgis;
    const conn = `PG:host=${postgis.host} user=${postgis.user} dbname=${postgis.dbname} password=${postgis.password}`;

    const mimetype = file.mimetype;
    const fileName = file.name;
    const tmpPath = `${process.cwd()}/tmp/${fileName}`;

    let archive = false;

    return new Promise( (resolve, reject) => {

        file.mv(tmpPath, async(err) => {

            if (err) throw err;

            let dataPath;

            if (mimetype === 'application/zip') {

                const files = await globWrapper('test', tmpPath);

                if (files.length) {

                    dataPath = files[0];

                } else {

                    resolve({
                        ok: 0,
                        message: "Archive doesn't contain *.shp file"
                    });

                }

            } else {

                dataPath = tmpPath;

            }


            const ogr = ogr2ogr(dataPath)
                .format('PostgreSQL')
                .options(['-nln', name])
                .skipfailures()
                .timeout(60000)
                .destination(conn)
                .stream();

            ogr.on('close', () => {

                rimraf('tmp/**/*', function() {

                    resolve({
                        ok: 1,
                    })

                });

            });

            ogr.on('error', (e) => {

                rimraf('tmp/**/*', function() {

                    console.log(e)

                    resolve({
                        ok: 0,
                        message: "An error occured. Probably something is wrong with the file."
                    })

                });

            });

        })

    });

}

export const publish = (name) => {

    const geoserver = config.geoserver;

    return new Promise( (resolve, reject) => {

        axios({
            method: 'POST',
            url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/datastores/${geoserver.store}/featuretypes`,
            auth: {
                username: geoserver.user,
                password: geoserver.password
            },
            headers: {
                'Content-Type': 'text/xml'
            },
            data: `<featureType><name>${name}</name></featureType>`

        })
        .then( response => {
            resolve({
                ok: 1,
                message: `Layer has been published.`
            })
        })
        .catch( err => {
            reject({
                ok: 0,
                message: err.response.data,
            })
        })

    })

}

export const precheck = (name) => {

    const postgis = config.postgis;
    const pool = new Pool({
      user: postgis.user,
      host: postgis.host,
      database: postgis.dbname,
      password: postgis.password,
      port: 5432,
    })

    return new Promise( (resolve, reject) => {

        pool.query(
            `
            SELECT EXISTS (
               SELECT 1
               FROM   pg_tables
               WHERE  schemaname = 'public'
               AND    tablename = '${name}'
            );
            `,
            (err, res) => {

              if (err) {
                  resolve(null);
              }

              resolve(res);

              pool.end();

            })

    })

}

const globWrapper = (name, tmpPath) => {

    return new Promise( (resolve, reject) => {

        extract(tmpPath, {dir: `${process.cwd()}/tmp/${name}`}, function (err) {

             if (err) {
                 reject();
             }

             glob(`tmp/${name}/*.shp`, {}, (err, files)=>{

                 if (err) {
                     reject();
                 } else {
                     resolve(files);
                 };

             });

        });

    });

}
