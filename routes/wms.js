import axios from 'axios';
import config from '../config/config';

export const getWmsStores = () => {

    const geoserver = config.geoserver;

    return new Promise( (resolve, reject) => {

        axios({
            method: 'GET',
            url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/wmsstores`,
            auth: {
                username: geoserver.user,
                password: geoserver.password
            }

        })
        .then( response => {
            resolve({
                ok: 1,
                data: response.data.wmsStores.wmsStore,
            })
        })
        .catch( err => {
            reject({
                ok: 0,
                message: err.response.data,
            })
        })

    })

}

export const getWmsStoreDescription = (name) => {

    const geoserver = config.geoserver;

    return new Promise( (resolve, reject) => {

        axios({
            method: 'GET',
            url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/wmsstores/${name}`,
            auth: {
                username: geoserver.user,
                password: geoserver.password
            }

        })
        .then( response => {
            resolve({
                ok: 1,
                data: response.data.wmsStore,
            })
        })
        .catch( err => {
            reject({
                ok: 0,
                message: err.response.data,
            })
        })

    })

}

export const getWmsStoreAvailable = (name) => {

    const geoserver = config.geoserver;

    return new Promise( (resolve, reject) => {

        axios({
            method: 'GET',
            url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/wmsstores/${name}/wmslayers?list=available`,
            auth: {
                username: geoserver.user,
                password: geoserver.password
            }

        })
        .then( response => {
            resolve({
                ok: 1,
                data: response.data.list.string,
            })
        })
        .catch( err => {
            reject({
                ok: 0,
                message: err.response.data,
            })
        })

    })

}

export const getWmsStorePublished = (name) => {

    const geoserver = config.geoserver;


    return new Promise( (resolve, reject) => {

        axios({
            method: 'GET',
            url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/wmsstores/${name}/wmslayers`,
            auth: {
                username: geoserver.user,
                password: geoserver.password
            }

        })
        .then( response => {
            resolve({
                ok: 1,
                data: response.data.wmsLayers.wmsLayer,
            })
        })
        .catch( err => {
            reject({
                ok: 0,
                message: err.response.data,
            })
        })

    })

}

export const getStoreData = async (req, res) => {

    try {
        const published = await getWmsStorePublished(req.params.store);
        const available = await getWmsStoreAvailable(req.params.store);
        const description = await getWmsStoreDescription(req.params.store);

        res.json({
            published: published.data,
            available: available.data,
            description: description.data,
            ok: 1
        });
    } catch(err) {
        res.json({
            ok: 0,
            name: req.params.store,
        })
    }

}

export const createWmsStore = (req, res) => {

    const name = req.body.name;
    const capabilities = req.body.capabilities.replace(/&/g,"%26");
    const description = req.body.description;

    const geoserver = config.geoserver;

    axios({
        method: 'POST',
        url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/wmsstores`,
        auth: {
            username: geoserver.user,
            password: geoserver.password
        },
        headers: {
            'Content-Type': 'text/xml'
        },
        data: `<wmsStore><name>${name}</name><description>${description}</description><type>WMS</type><capabilitiesURL>${capabilities}</capabilitiesURL></wmsStore>`

    })
    .then( response => {

        res.redirect('/admin/webservices')

    })
    .catch( err => {

        res.json({
            ok: 0,
            message: err.response.data,
        })

    });

}

export const deleteWmsStore = (req, res) => {

    const name = req.params.name;

    const geoserver = config.geoserver;

    axios({
        method: 'DELETE',
        url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/wmsstores/${name}?recursive=true`,
        auth: {
            username: geoserver.user,
            password: geoserver.password
        },
        headers: {
            'Content-Type': 'application/json'
        }

    })
    .then( response => {

        res.json({
            ok: 1
        })

    })
    .catch( err => {

        res.json({
            ok: 0,
            message: err.response.data,
        })

    });

}

export const publishWmsLayer = (req, res) => {

    const geoserver = config.geoserver;
    const store = req.body.store;
    const layer = req.body.layer;
    const name = req.body.name;

    return new Promise( (resolve, reject) => {

        axios({
            method: 'POST',
            url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/wmsstores/${store}/wmslayers`,
            auth: {
                username: geoserver.user,
                password: geoserver.password
            },
            headers: {
                'Content-Type': 'text/xml'
            },
            data: `<wmsLayer><nativeName>${layer}</nativeName><name>${name}</name></wmsLayer>`

        })
        .then( response => {
            res.json({
                ok: 1,
                message: `Layer has been published.`
            })
        })
        .catch( err => {
            res.json({
                ok: 0,
                message: err.response.data,
            })
        })

    })

}

export const unpublishWmsLayer = (req, res) => {

    const geoserver = config.geoserver;
    const store = req.params.store;
    const layer = req.params.layer;

    return new Promise( (resolve, reject) => {

        axios({
            method: 'DELETE',
            url: `${geoserver.url}/geoserver/rest/workspaces/${geoserver.namespace}/wmslayers/${layer}.json?recurse=true`,
            auth: {
                username: geoserver.user,
                password: geoserver.password
            },
            headers: {
                'Content-Type': 'text/xml'
            }


        })
        .then( response => {
            res.json({
                ok: 1,
                message: `Layer has been unpublished.`
            })
        })
        .catch( err => {
            res.json({
                ok: 0,
                message: err.response.data,
            })
        })

    })
}
