import MapComposition from '../models/map-composition';

export const createMapComposition = async(req, res) => {

    const indicators = req.body.indicators;
    const basemap = req.body.basemap;
    const extent = req.body.extent;

    const composition = new MapComposition();
    composition.indicators = indicators;
    composition.basemap = basemap;
    composition.extent = extent;

    const result = await composition.save();

    res.json(result._id);

}

export const getMapComposition = async(req, res) => {

    const id = req.params.id;

    const composition = await MapComposition.findOne({_id: id});

    res.json(composition);

}
