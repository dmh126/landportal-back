import app from '../app';
import chai, {assert} from 'chai';
import _ from 'lodash';

chai.should();

// Test datasets api

describe('Layers Api', () => {



  it('Get layers list helper', (done) => {
    let a = [
        {id: 'aaa', v: 1, p: true}, {id: 'bbb', v: 2, p: true}
    ];
    let b = [
        {id: 'aaa', v: 1}, {id: 'bbb', v: 2}, {id: 'ccc', v: 3}
    ];

    const c = _.unionWith(a,b, (x,y) => x.id === y.id )

    console.log(c)
    c.should.be.an('array');
    done()
  });

})
