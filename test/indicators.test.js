import app from '../app';
import chai, {assert} from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);
chai.should();

// models
import Indicator from '../models/indicator';
import Dataset from '../models/dataset';

// Test datasets api

describe('Indicators Api', () => {

  let indicator;
  let dataset;

  before( async() => {

          const ind = await Indicator.findOne({});
          const ds = await Dataset.findOne({});
          indicator = ind;
          dataset = ds;
          
  });

  it('Get indicators by dataset', (done) => {
    chai.request(app)
      .get(`/api/v1/indicators/dataset/${dataset._id}`)
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('array');
          done();
      });
  });

  it('Get indicators by dataset (wrong id)', (done) => {
    chai.request(app)
      .get(`/api/v1/indicators/dataset/brokenid`)
      .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.an('array');
          done();
      });
  });

  it('Get one indicator', (done) => {
    chai.request(app)
      .get(`/api/v1/indicator/${indicator._id}`)
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('object');
          assert.equal(res.body.indicator._id, indicator._id);
          done();
      });
  });

  it('Get one indicator (wrong id)', (done) => {
    chai.request(app)
      .get(`/api/v1/indicator/brokenid`)
      .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.an('object');
          done();
      });
  });

  it('Get indicators by theme', (done) => {
    chai.request(app)
      .post(`/api/v1/indicators/theme`)
      .send({
          'theme': 'Land Conflicts',
      })
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('array');
          done();
      });
  });

  it('Get indicators by country', (done) => {
    chai.request(app)
      .post(`/api/v1/indicators/country`)
      .send({
          'country': 'BFA',
      })
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('array');
          done();
      });
  });

  it('Search advanced', (done) => {
    chai.request(app)
      .post(`/api/v1/indicators/advanced`)
      .send({
          'query': 'Land conflicts',
      })
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('object');
          res.body.should.have.property('indicators');
          res.body.should.have.property('themes');
          res.body.should.have.property('countries');
          res.body.should.have.property('datasets');
          done();
      });
  });

  it('Search advanced (no query)', (done) => {
    chai.request(app)
      .post(`/api/v1/indicators/advanced`)
      .send({
          'query': '',
      })
      .end((err, res) => {
          res.should.have.status(200);
          assert.equal(res.body.ok, 0);
          done();
      });
  });


})
