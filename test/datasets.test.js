import app from '../app';
import chai, {assert} from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);
chai.should();

// models
import Dataset from '../models/dataset';

// Test datasets api

describe('Datasets Api', () => {

  let dataset;

  before( async() => {

      const ds = await Dataset.findOne({});
      dataset = ds;

  });

  it('Get datasets list', (done) => {
    chai.request(app)
      .get('/api/v1/datasets')
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('array');
          done();
      });
  });

  it('Get one dataset', (done) => {
    chai.request(app)
      .get(`/api/v1/dataset/${dataset._id}`)
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('object');
          assert.equal(res.body.dataset._id, dataset._id);
          done();
      });
  });

  it('Get one dataset (wrong id)', (done) => {
    chai.request(app)
      .get(`/api/v1/dataset/FAO-ASTI`)
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('object');
          done();
      });
  });


  it('Get themes array', (done) => {
    chai.request(app)
      .get(`/api/v1/themes`)
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('themes');
          res.body.themes.should.be.an('array');
          done();
      });
  });

  it('Get concepts array', (done) => {
    chai.request(app)
      .get(`/api/v1/concepts`)
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('concepts');
          res.body.concepts.should.be.an('array');
          done();
      });
  });


})
/*
describe('simple POST', function() {
  it('should return user id', function(done) {
    request(app)
      .post('/api/v1/login')
      .send({"email":"test@gmail.com", "password": "test"})
      .expect(200)
      .expect('Content-Type', /json/)
      .expect(function(res){
        res.body.should.be.instanceof(String);
      })
      .end(done);
  })
})*/
