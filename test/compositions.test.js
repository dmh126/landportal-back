import app from '../app';
import chai, {assert} from 'chai';
import chaiHttp from 'chai-http';

chai.use(chaiHttp);
chai.should();

// models
import MapComposition from '../models/map-composition';

// Test datasets api

describe('Compositions Api', () => {

  let composition;

  before( async() => {

      const cmp = await MapComposition.findOne({});
      composition = cmp;

  });

  it('Get one composition', (done) => {
    chai.request(app)
      .get(`/api/v1/compositions/${composition._id}`)
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('object');
          done();
      });
  });

})
