import app from '../app';
import chai, {assert} from 'chai';
import chaiHttp from 'chai-http';
import {grabLayers} from '../routes/helpers'

chai.use(chaiHttp);
chai.should();

// Test datasets api

describe('Layers Api', () => {


  it('Get layers list', (done) => {
    chai.request(app)
      .get(`/admin/layers`)
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an('array');
          done();
      });
  });

  it('Get layers list helper', async() => {
    const res = await grabLayers();
    res.should.be.an('array');
  });

})
