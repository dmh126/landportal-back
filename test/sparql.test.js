import app from '../app';
import chai, {assert} from 'chai';
import chaiHttp from 'chai-http';

import {
    getAllDatasetsBySparql,
    getIndicatorsBySparql,
    getOneIndicatorBySparql,
    getOneDatasetBySparql,
    getIndicatorsAdvanced,
    getIndicatorCountries,
    getThemes,
    getConcepts
} from '../routes/sparql.js';

chai.use(chaiHttp);
chai.should();

// Test datasets api

describe('SPARQL Api', () => {

    it('Get datasets list', async() => {

        const datasets = await getAllDatasetsBySparql();
        chai.expect(datasets).to.be.an('array');
        assert.isNotEmpty(datasets);
        assert.containsAllKeys(datasets[0], ['id', 'label']);

    });

    it('Get indicators by dataset id', async() => {

        const indicators = await getIndicatorsBySparql('FAO-ASTI');
        chai.expect(indicators).to.be.an('array');
        assert.isNotEmpty(indicators);
        assert.containsAllKeys(indicators[0], ['id', 'label']);

    });

    it('Get dataset meta', async() => {

        const dataset = await getOneDatasetBySparql('FAO-ASTI');
        chai.expect(dataset).to.be.an('array');
        assert.containsAllKeys(dataset[0], ['id', 'label']);
        assert.isNotEmpty(dataset);

    });

    it('Get indicator meta', async() => {

        const indicators = await getOneIndicatorBySparql('FAO-23046-6085');
        chai.expect(indicators).to.be.an('array');
        assert.isNotEmpty(indicators);
        assert.containsAllKeys(indicators[0], ['id', 'label']);

    });

    it('Get indicators advanced', async() => {

        const indicators = await getIndicatorsAdvanced();
        chai.expect(indicators).to.be.an('array');
        assert.isNotEmpty(indicators);
        assert.containsAllKeys(indicators[0], ['id', 'label']);

    });

    it('Get indicator countries', async() => {

        const indicators = await getIndicatorCountries('FAO-23046-6085');
        chai.expect(indicators).to.be.an('array');
        assert.isNotEmpty(indicators);
        assert.containsAllKeys(indicators[0], ['country']);

    });

    it('Get themes', async() => {

        const indicators = await getThemes();
        chai.expect(indicators).to.be.an('array');
        assert.isNotEmpty(indicators);
        assert.containsAllKeys(indicators[0], ['id', 'label']);

    });

    it('Get concepts', async() => {

        const indicators = await getConcepts();
        chai.expect(indicators).to.be.an('array');
        assert.isNotEmpty(indicators);
        assert.containsAllKeys(indicators[0], ['concept', 'label']);

    });

})
