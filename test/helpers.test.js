import app from '../app';
import chai, {assert} from 'chai';
import chaiHttp from 'chai-http';

import {
    getOneIndicatorBySparql,
    getOneDatasetBySparql
} from '../routes/sparql.js';

import {
    buildIndicatorFromSparql,
    buildDatasetFromSparql,
    buildIndicatorsList,
    buildDatasetsList
} from '../routes/datasets.js';

import {
    fixTagsSemicolons
} from '../routes/helpers.js';


chai.use(chaiHttp);
chai.should();

// Test datasets api

describe('Helpers functions', () => {

    it('Build dataset from SPARQL bindings', async() => {

        const dataset = await getOneDatasetBySparql('FAO-ASTI');
        const build = buildDatasetsList(dataset);
        chai.expect(build).to.be.an('array');
        chai.expect(build).to.have.lengthOf(1);
        const first = build[0];
        assert.containsAllKeys(first, ['_id', 'name', 'about', 'meta']);
        chai.expect(first.meta).to.be.an('object');

    });

    it('Build dataset from SPARQL bindings', async() => {

        const indicator = await getOneIndicatorBySparql('FAO-23046-6085');
        const build = buildIndicatorsList(indicator);
        chai.expect(build).to.be.an('array');
        chai.expect(build).to.have.lengthOf(1);
        const first = build[0];
        assert.containsAllKeys(first, ['_id', 'name', 'about', 'meta']);
        chai.expect(first.meta).to.be.an('object');

    });

    it('Fix themes & concepts passed into jQuery form', async() => {
        const themes = `Urban Tenure, Land, Climate Change & Environment`;
        const concepts = `land, land tenure, water management`;
        const resultThemes = await fixTagsSemicolons(themes, 'themes');
        //const resultConcepts = await fixTagsSemicolons(concepts, 'concepts');
        chai.expect(resultThemes).to.be.a('string');
        //chai.expect(resultConcepts).to.be.a('string');
        assert.equal(resultThemes, 'Urban Tenure;Land, Climate Change & Environment');
        //assert.equal(resultConcepts, 'land tenure;water management;land');
    });


})
