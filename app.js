import {} from 'dotenv/config';
import express from 'express';
import path from 'path';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';
import passport from 'passport';
import session from 'express-session';
import connectMongo from 'connect-mongo';
import config from './config/config';
import setupPassport from './authorization/authorization';
import createRoutes from './routes/index';
import generateSeed from './config/seed';
//import upload from 'express-fileupload';

const MongoStore = connectMongo(session);

const app = express();

mongoose.connect(
    config.database.url,
    {
        useMongoClient: true,
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000

    }
);

mongoose.Promise = require('bluebird');

setupPassport(passport);

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(cors());
//app.use(upload());
app.set('view engine', 'ejs');

app.use(session({
  secret: config.database.secret,
  resave: true,
  saveUninitialized: true,
  store: new MongoStore({ mongooseConnection: mongoose.connection, collection: 'sessions'}),
  cookie: { maxAge: 3600000 }
 }));
app.use(passport.initialize());
app.use(passport.session());

createRoutes(app, passport);

// seed if development or test
let node_env = process.env.NODE_ENV
if ( node_env !== 'production' ) {

  generateSeed()

}

// serve static files
app.use('/static', express.static(path.join(__dirname, 'static')));

// favicon workaround
app.get('/favicon.ico', (req, res) => res.status(204));

// catch 404 and forward to error handler
app.use( (req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use( (err, req, res, next) => {
  // set locals, only providing error in development
  const env = req.app.get('env');
  res.locals.message = err.message;
  res.locals.error = env === 'development' ? err : {};

  if (env !== 'test') console.log(err);

  // render the error page
  res.status(err.status || 500);
  res.send({
    message: res.locals.message,
    error: res.locals.error
  });
});

export default app;
